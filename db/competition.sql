# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.43-0ubuntu0.14.04.1)
# Database: competitions.cnc.co.za
# Generation Time: 2016-04-05 10:14:39 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table wp_competition_entrant_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_competition_entrant_details`;

CREATE TABLE `wp_competition_entrant_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `email` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(10) NOT NULL DEFAULT '',
  `interests` text NOT NULL,
  `location` varchar(100) NOT NULL DEFAULT '',
  `optin` tinyint(1) NOT NULL DEFAULT '0',
  `can_spin` tinyint(1) NOT NULL DEFAULT '1',
  `source` varchar(75) DEFAULT NULL,
  `last_fb_share` datetime DEFAULT NULL,
  `last_twitter_share` datetime DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wp_competition_entrant_friends
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_competition_entrant_friends`;

CREATE TABLE `wp_competition_entrant_friends` (
  `entrant_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(75) NOT NULL DEFAULT '',
  `entered` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`entrant_id`,`email`),
  CONSTRAINT `wp_competition_entrant_friends_ibfk_1` FOREIGN KEY (`entrant_id`) REFERENCES `wp_competition_entrant_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wp_competition_social_shares
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_competition_social_shares`;

CREATE TABLE `wp_competition_social_shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entrant_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL DEFAULT '',
  `earned_spin` int(11) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wp_competition_vouchers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_competition_vouchers`;

CREATE TABLE `wp_competition_vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(75) NOT NULL,
  `type` varchar(75) NOT NULL,
  `subtype` varchar(75) DEFAULT NULL,
  `value` varchar(25) NOT NULL DEFAULT '',
  `winner_id` int(11) DEFAULT NULL,
  `won_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winner_id` (`winner_id`),
  CONSTRAINT `wp_competition_vouchers_ibfk_1` FOREIGN KEY (`winner_id`) REFERENCES `wp_competition_entrant_details` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wp_competitions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_competitions`;

CREATE TABLE `wp_competitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `name` varchar(75) NOT NULL,
  `details` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wp_competitions` WRITE;
/*!40000 ALTER TABLE `wp_competitions` DISABLE KEYS */;

INSERT INTO `wp_competitions` (`id`, `slug`, `name`, `details`, `weight`)
VALUES
	(3,'EntertainerApps','Win 1 of 60 Entertainer Apps','A CHANCE TO GET YOUR HANDS ON 1 OF 60 ENTERTAINER APPS',1),
	(4,'Mauritius','Win a trip for 2 to Mauritius','A CHANCE TO GET YOUR HANDS ON A 5 NIGHT ALL-INCLUSIVE TRIP FOR TWO TO MAURITIUS',3),
	(5,'PC-FitBit','Win a FitBit from PriceCheck','A CHANCE TO GET YOUR HANDS ON A FITBIT CHARGE LARGE ACTIVITY TRACKER',1),
	(6,'PC-Tablet','Win a Tablet from PriceCheck','A CHANCE TO GET YOUR HANDS ON A SAMSUNG GALAXY TAB 4 10.1\'\' WITH WIFI & 3G',1),
	(7,'PC-GoPro','Win a GoPro from PriceCheck','A CHANCE TO GET YOUR HANDS ON A GOPRO HERO ACTION CAMERA',1),
	(8,'PC-Cloud9','Win a Cloud 9 from PriceCheck','A CHANCE TO GET YOUR HANDS ON A CLOUD NINE ORIGINAL IRON',1),
	(9,'Flook','Win a Flook golf trip','A CHANCE TO GET YOUR HANDS ON A 4-DAY GOLF TOUR TO THE SAN LAMEER HOTEL & SPA',1),
	(10,'Kidzone-Mixed','Win 1 of 22 Kidzone prizes','A CHANCE TO GET YOUR HANDS ON 1 OF 15 MIXED BRAND BOX HAMPERS',1),
	(11,'Kidzone-Sofia','Win 1 of 22 Kidzone prizes','A CHANCE TO GET YOUR HANDS ON 1 OF 5 \'\'SOFIA THE FIRST\'\' HAMPERS',1),
	(12,'Kidzone-Voucher','Win 1 of 22 Kidzone prizes','A CHANCE TO GET YOUR HANDS ON A R500 VOUCHER FROM KIDZONE',1);

/*!40000 ALTER TABLE `wp_competitions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_competitions_entrants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_competitions_entrants`;

CREATE TABLE `wp_competitions_entrants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competition_id` int(11) NOT NULL,
  `entrant_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `competition_id` (`competition_id`),
  KEY `entrant_id` (`entrant_id`),
  CONSTRAINT `wp_competitions_entrants_ibfk_2` FOREIGN KEY (`entrant_id`) REFERENCES `wp_competition_entrant_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wp_competitions_entrants_ibfk_3` FOREIGN KEY (`competition_id`) REFERENCES `wp_competitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
