CREATE TABLE IF NOT EXISTS `wp_competition_entrant_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `email` varchar(50) NOT NULL,
  `optin` tinyint(1) NOT NULL DEFAULT 0,
  `can_spin` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `wp_competition_vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(75) NOT NULL,
  `type` varchar(75) NOT NULL,
  `value` varchar(25) NOT NULL,
  `winner_id` int(11) DEFAULT NULL,
  `won_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX (`winner_id`),
  FOREIGN KEY (`winner_id`) REFERENCES `wp_competition_entrant_details` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `wp_competitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `name` varchar(75) NOT NULL,
  `details` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `wp_competitions` (`id`, `slug`, `name`, `details`)
VALUES
  (1,'ps4','Win a PS4!!','You stand a chance to win one of 4 PS4\'s'),
  (2,'one-day-only','Win a R5000 OneDayOnly Voucher','You stand a chance to win a voucher for OneDayOnly to the value of R5 000!');

CREATE TABLE IF NOT EXISTS `wp_competitions_entrants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competition_id` int(11) NOT NULL,
  `entrant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `competition_id` (`competition_id`),
  KEY `entrant_id` (`entrant_id`),
  FOREIGN KEY (`entrant_id`) REFERENCES `wp_competition_entrant_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`competition_id`) REFERENCES `wp_competitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
