<?php

/*----------------------------------------------------*/
// Local environment vars
/*----------------------------------------------------*/
return array(

    'DB_NAME'       => 'playspinandwin', // competitions.cnc.co.za
    'DB_USER'       => 'root',
    'DB_PASSWORD'   => '',
    'DB_HOST'       => 'localhost',
    'WP_HOME'       => 'http://competitions.clickncompare.co.za',
    'WP_SITEURL'    => 'http://competitions.clickncompare.co.za/cms'

);
