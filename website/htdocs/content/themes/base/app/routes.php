<?php

/*
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */

Route::get('page', function(){
    return View::make('desktop/pages/competitionend');
});

Route::post('page', 'Controller\PagesController@competition_end');

/*Route::get('page', ['mailer-test', function()
 {
     return View::make('mailer.friend-spin');
 }]);


// Pages Routing
Route::get('front', 'Controller\SpinController@home');
Route::any('api/signup', 'Controller\ApiController@signup');
Route::any('api/spin', 'Controller\ApiController@spin');
Route::any('api/addFriend', 'Controller\ApiController@addFriend');
Route::any('api/socialShare', 'Controller\ApiController@socialShare');
Route::any('api/downloadAuth', 'Controller\ApiController@downloadAuth');
Route::any('win', 'Controller\SpinController@win');
Route::any('sorry', 'Controller\SpinController@sorry');
Route::any('data-dump', 'Controller\SpinController@dataAuth');
Route::any('download-data', 'Controller\SpinController@downloadData');
//Route::any('spin', 'Controller\SpinController@spin');

Route::get('page', ['mailers', function()
{
    return View::make('mailer.friend-invite');
}]);

// MAILER :: prize
Route::get('page', ['mailers/prize', function()
{
    return View::make('mailer.prize');
}]);

// MAILER :: boxes
Route::get('page', ['mailers/boxes', function()
{
    return View::make('mailer.boxes');
}]);

// MAILER :: iron
Route::get('page', ['mailers/iron', function()
{
    return View::make('mailer.iron');
}]);

// MAILER :: gopro
Route::get('page', ['mailers/gopro', function()
{
    return View::make('mailer.gopro');
}]);

// MAILER :: galaxy
Route::get('page', ['mailers/galaxy', function()
{
    return View::make('mailer.galaxy');
}]);

// MAILER :: fitbit
Route::get('page', ['mailers/fitbit', function()
{
    return View::make('mailer.fitbit');
}]);

// MAILER :: sofia
Route::get('page', ['mailers/sofia', function()
{
    return View::make('mailer.sofia');
}]);

// MAILER :: flookgolf
Route::get('page', ['mailers/flookgolf', function()
{
    return View::make('mailer.flookgolf');
}]);

// MAILER :: eapp
Route::get('page', ['mailers/eapp', function()
{
    return View::make('mailer.eapp');
}]);

// MAILER :: mauritius
Route::get('page', ['mailers/mauritius', function()
{
    return View::make('mailer.mauritius');
}]);

// MAILER :: camwarehouse
Route::get('page', ['mailers/camwarehouse', function()
{
    return View::make('mailer.camwarehouse');
}]);

// MAILER :: netflorist
Route::get('page', ['mailers/netflorist', function()
{
    return View::make('mailer.netflorist');
}]);

// MAILER :: ucook
Route::get('page', ['mailers/ucook', function()
{
    return View::make('mailer.ucook');
}]);

// MAILER :: ccvoucher_minvalue
Route::get('page', ['mailers/ccvoucher_minvalue', function()
{
    return View::make('mailer.ccvoucher_minvalue');
}]);

// MAILER :: ccvoucher
Route::get('page', ['mailers/ccvoucher', function()
{
    return View::make('mailer.ccvoucher');
}]);

/*Route::get('page', ['faq', function()
{
    return View::make('desktop.pages.faq');
}]);*

Route::get('page', ['terms', function()
{
    return View::make('desktop.pages.terms');
}]);

Route::get('page', ['help', function()
{
    return View::make('desktop.pages.faq');
}]);
 

/*

Route::any('julian', function(){
	return \View::make('desktop.pages.spin.home');
});


Route::get('page', array('home', 'uses' => 'PagesController@home'));
Route::get('page', array('about', 'uses' => 'PagesController@about'));
Route::get('page', array('signup', 'uses' => 'PagesController@signup'));
Route::get('page', "PagesController@competition");
Route::get('search', "PagesController@Search");
Route::get( '404', function() {
       wp_redirect( get_home_url(), 302 );
});

// Episodes Routing
Route::get('postTypeArchive', array('eps-episodes', 'uses' => 'EpisodesController@archive'));
Route::get('singular', array('eps-episodes', 'uses' => 'EpisodesController@single'));

// Recipes Routing
Route::get('postTypeArchive', array('rcps-recipes', 'uses' => 'RecipesController@archive'));
Route::get('singular', array('rcps-recipes', 'uses' => 'RecipesController@single'));

// Blog Routing
Route::get('home', 'BlogController@archive');
Route::get('category', 'BlogController@archive');
Route::get('singular', array('post', 'uses' => 'BlogController@single'));

// Contestant Routing
Route::get('postTypeArchive', array('ctst-contestants', 'uses' => 'ContestantsController@archive'));
Route::get('singular', array('ctst-contestants', 'uses' => 'ContestantsController@single'));

// feature nav

$detect = new Mobile_Detect;

// Feature Phone Routing

if ($detect->isMobile() && $detect->mobileGrade() === "C" ) {

    Route::any('page', array('menu', function(){
    		return View::make('mobile.includes.navigation');
    }));

}

elseif (!$detect->isMobile() ) {

    Route::any('page', array('menu', function(){
    		wp_redirect( get_home_url(), 302 );
    }));

}
*/
