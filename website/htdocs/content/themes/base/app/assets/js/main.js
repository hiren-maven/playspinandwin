
/* -------------------------------------------------------------
    FUNCTIONS
------------------------------------------------------------- */

//Get Facebook App ID depending on environment
// function getFacebookAppId(){
//     var fbAppId = "";
//     switch(window.location.hostname) {
//         //LOCAL
//         case "mysite.dev":
//             fbAppId = "xxxxxxxxxxxxxxxx";
//             break;
//         //STAGING
//         case "mysite.liqstage.co.za":
//             fbAppId = "xxxxxxxxxxxxxxxx";
//             break;
//         //LIVE
//         default:
//             fbAppId = "504199742961488";
//     }
//     return fbAppId;
// }

//console.log fix for old IE
if (typeof console == "undefined") {
    this.console = {log: function() {}};
}

//Google Analytics Event Tracking helper
function trackEvent(action) {
    if(typeof _gaq != "undefined") {
        _gaq.push(["_trackEvent", "Cycle", action]);
    } else {
        console.log("Tried to track event '"+action+"', but Google Analytics is not installed.");
    }
}

//Fill form with Facebook details
function fillForm(fbData) {
    trackEvent("Facebook login");

    $("#first_name").val(fbData.first_name || "");
    $("#last_name").val(fbData.last_name || "");
    $("#email").val(fbData.email || "");
}



/* -------------------------------------------------------------
    Wordpress Doc Ready
------------------------------------------------------------- */
(function($) {
    
    // Home Carousels

    var owlhome = $('#home-carousel');
    var owlcontestant = $('.contestant-carousel');

    owlhome.owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: [
              "<i class='home_nav prev'></i>",
              "<i class='home_nav next'></i>"
              ],
        items: 1,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    owlcontestant.owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: [
              "<i class='home_nav contestant_nav prev'></i>",
              "<i class='home_nav contestant_nav next'></i>"
              ],
        items: 3,
        responsive:{
            300: {
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

    // Custom Navigation Events
    $(".next").click(function(){
        owlhome.trigger('owl.next');
    });

    $(".prev").click(function(){
        owlhome.trigger('owl.prev');
    });

    $('.search-button').click(function() {
        $('.search-form').toggle()
    });
    
    if ($(".footer img").css("float") == "left" ){
        if ($("body").hasClass("recipes") || $("body").hasClass("episodes") || $("body").hasClass("tips") || $("body").hasClass("blog")) {
            window.addEventListener("scroll",function() {
                $('.navbar-fixed-top')[(window.scrollY > 150)?"slideDown":"slideUp"]();
            },false);
        }
    
        if ($("body").hasClass("home")) {
            window.addEventListener("scroll",function() {
                $('.navbar-fixed-top')[(window.scrollY > 600)?"slideDown":"slideUp"]();
            },false);
        }
    }

})(jQuery);
