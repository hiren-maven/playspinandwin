function tweetCurrentPage() {
    window.open('https://twitter.com/share?url='+escape(window.location.href)+'&text='+document.title + ' via @' + twitterHandle, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    return false;
}

jQuery.validator.addMethod("genderChanged", function(value, element) {
    return value != 'Gender';
}, "*You must select a gender");

jQuery.validator.addMethod("locationChanged", function(value, element) {
    return value != 'Location';
}, "*You must select a location");

var saveSocialShare = function(shareType) {
    console.log('#### social Share: ' + shareType);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: 'api/socialShare/',
        data: JSON.stringify({platform:shareType}),
        error: function(data){
            // handle redirects
            //console.log()
            window.location = window.location.protocol + '//' + window.location.host + '/sorry';
        },
        success: function(data){
            // success
            // spin wheel
            //console.log('###### Success');
            window.location = window.location.protocol + '//' + window.location.host;

        },
        complete: function(data) {
            //console.log('######## Complete');
            //console.log(data);
        },
        dataType: "json"
    });
};
 /**
     * Parses URL query string and returns all parameters as an object.
     *
     * @returns {{}}
     */
    function getURLParams() {
        var params = {};
        var query = window.location.search.substring(1);
        var pieces = query.split('&');
        for (var i = 0; i < pieces.length; i++) {
            var parts = pieces[i].split('=');
            var key = parts[0];
            var value = parts[1];

            // skip empty keys, empty values are more acceptable
            if (key.length == 0) {
                continue;
            }

            if (typeof params[key] == 'undefined') {
                params[key] = decodeURIComponent(value);
            } else if (typeof params[key] == 'string') {
                params[key] = [
                    params[key],
                    decodeURIComponent(value)
                ];
            } else {
                params[key].push(decodeURIComponent(value));
            }
        }

        return params;
    }
$(document).ready(function () {

    if(location.href.indexOf("sorry") > -1){
      $("body").addClass("bg-rightupagainstpage");
    } else if(location.href.indexOf("win") > -1) {
      $("body").addClass("win-bg-rightupagainstpage");
    }

    $('#interests').multiselect({
            numberDisplayed: 1
        });

    $('.checkboxwrapper input:checkbox').screwDefaultButtons({
      image: 'url("/content/themes/base/app/assets/images/switch-state.png")',
      width: 12,
      height: 12
    });

    var $val = $("#entry-form").submit(function(e) {
        //stop the form from submiting and submit with ajax
        e.preventDefault();
    }).validate({
      rules: {
          name: {
              required: true
          },
          email: {
              required: true,
              email: true
          },
          age: {
              required: true
          },
          gender: {
              required: true,
              genderChanged: true
          },
          interests: {
              required: true
          },
          location: {
              required: true,
              locationChanged: true
          },
          contactno: {
              required: true,
              minlength: 10,
              maxlength: 10
          },
          spintermcheck: {
              required: true
          }
      },
      messages: {
          name: "*This field is required",
          email: "*Please fill in a valid email address",
          age: "*This field is required",
          gender: {
              required: "*This field is required",
              genderChanged: "*You must select a gender"
          },
          interests: "*This field is required",
          location: {
              required: "*This field is required",
              locationChanged: "*You must select a location"
          },
          contactno:{
            required:'* This field is required',
          }
      },errorPlacement: function(error, element) {
            if (element.attr("name") == "spintermcheck") {
                error.appendTo(".spin-check");
            } else {
                error.insertAfter(element);
            }
        },

      submitHandler: function(form) {

		      //Disable the submit button
          $("#main-submit").attr("disabled", true);

          /*$(".spinning-wheel-wrapper, #new-spin-btn").show();*/

          // awesomeness check
         /* var awesome = $('input[name="company"]').val();
          if (awesome.length > 0) {
              return false;
          }*/

          var jsonData = {};

          // check for source
          var sourceKey = 'utm_campaign';
          var params = getURLParams();

          if (typeof params[sourceKey] != 'undefined') {
              if (typeof params[sourceKey] == 'object') {
                  jsonData.source = params[sourceKey].toString();
              } else {
                  jsonData.source = params[sourceKey];
              }
          } else {
              jsonData.source = "web";
          }

          // changing the request parameter name from 'name' to 'fullname' due to 404 concerns, please leave as is
          jsonData.fullname = $('input[name="name"]').val();
          jsonData.email = $('input[name="email"]').val();
          jsonData.age = $('input[name="age"]').val();
          jsonData.gender = $('select[name="gender"]').val();
          jsonData.interests = $('select[name="interests"]').val();
          jsonData.location = $('select[name="location"]').val();
          jsonData.contactno = $('input[name="contactno"]').val();
          jsonData.friendname = $('input[name="friend_name"]').val();
          jsonData.friendemail = $('input[name="friend_email"]').val(); 
          //jsonData.optin = $('input[name="optin"]').prop('checked');

          $.ajax({
              type: "POST",
              contentType: "application/json",
              url: $("#entry-form").attr('action'),
              data: JSON.stringify(jsonData),
              error: function(data){

				  //Re-enable the submit button
				  $("#main-submit").attr("disabled", false);


                  // handle redirects
                  if (typeof data.responseJSON != 'undefined' && typeof data.responseJSON.redirect != 'undefined' && data.responseJSON.redirect.length > 0) {
                      window.location = data.responseJSON.redirect;
                      return;
                  }

                  if (typeof data.responseJSON.validation != 'undefined') {
                      // show validation errors
                      var $validator = $("#entry-form").validate();
                      try {
                          $validator.showErrors(data.responseJSON.validation);
                      }
                      catch (e) {
                          var errors = {"mobile": data.error.message};
                          $validator.showErrors(errors);
                      }
                  }
              },
              success: function(data){
                  // success
                  // spin wheel
                  if (typeof data.prize != 'undefined' && data.prize.wheelIndex != 'undefined') {
                      var wheelIndex = data.prize.wheelIndex;
                      if (Number(wheelIndex) === wheelIndex && wheelIndex % 1 === 0) {
                          startSpin(wheelIndex, function() {
                              setTimeout(function() {
                                  var location = '/win';

                                  if (typeof data.redirect != 'undefined' && data.redirect.length > 0) {
                                      location = data.redirect;
                                  }
                                  if (typeof data.prize != 'undefined' && typeof data.prize.slug != 'undefined') {
                                      location += '?prize=' + data.prize.slug;
                                  }

                                  window.location = location;
                              }, 1500);
                          });
                      }
                  }
              },
              complete: function(data) {
                  //console.log(data);
              },
              dataType: "json"
          });
      },
      ignore: ''
    });

    var addFriendForm = $("#add-friend-form").submit(function(e) {
        //console.log('form submit');
        //stop the form from submitting and submit with ajax
        e.preventDefault();
    }).validate({
        rules: {
            friend_name_1: {
                required: true
            },
            friend_email_1: {
                required: true,
                email: true
            },
            friend_name_2:{
               required: {
                   depends: function(element) {
                       return $('input[name="friend_email_2"]').val() != "";
                   }
               }
            },
            friend_email_2: {
                email: true,
                required: {
                    depends: function(element) {
                        return $('input[name="friend_name_2"]').val() != "";
                    }
                }
            },
            friend_name_3:{
               required: {
                   depends: function(element) {
                       return $('input[name="friend_email_3"]').val() != "";
                   }
               }
            },
            friend_email_3: {
                email: true,
                required: {
                    depends: function(element) {
                        return $('input[name="friend_name_3"]').val() != "";
                    }
                }
            },
            friend_name_4:{
               required: {
                   depends: function(element) {
                       return $('input[name="friend_email_4"]').val() != "";
                   }
               }
            },
            friend_email_4: {
                email: true,
                required: {
                    depends: function(element) {
                        return $('input[name="friend_name_4"]').val() != "";
                    }
                }
            },
            friend_name_5:{
               required: {
                   depends: function(element) {
                       return $('input[name="friend_email_5"]').val() != "";
                   }
               }
            },
            friend_email_5: {
                email: true,
                required: {
                    depends: function(element) {
                        return $('input[name="friend_name_5"]').val() != "";
                    }
                }
            }
        },
        messages: {
            friend_name_1: "Name is required",
            friend_email_1: "Email is required",
            friend_name_2: "Name is required",
            friend_email_2: "Email is required",
            friend_name_3: "Name is required",
            friend_email_3: "Email is required",
            friend_name_4: "Name is required",
            friend_email_4: "Email is required",
            friend_name_5: "Name is required",
            friend_email_5: "Email is required"
        },

        submitHandler: function(form) {
            var jsonData = {};
            // populate jsonData
            jsonData.source = "web";

			//Disable the submit button
			$("input[type=submit]").attr("disabled", true);

            //KISS - five sets of fields ..
            jsonData.friend_name_1 = $('input[name="friend_name_1"]').val();
            jsonData.friend_email_1 = $('input[name="friend_email_1"]').val();
            jsonData.friend_name_2 = $('input[name="friend_name_2"]').val();
            jsonData.friend_email_2 = $('input[name="friend_email_2"]').val();
            jsonData.friend_name_3 = $('input[name="friend_name_3"]').val();
            jsonData.friend_email_3 = $('input[name="friend_email_3"]').val();
            jsonData.friend_name_4 = $('input[name="friend_name_4"]').val();
            jsonData.friend_email_4 = $('input[name="friend_email_4"]').val();
            jsonData.friend_name_5 = $('input[name="friend_name_5"]').val();
            jsonData.friend_email_5 = $('input[name="friend_email_5"]').val();

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: $("#add-friend-form").attr('action'),
                data: JSON.stringify(jsonData),
                error: function(data){

					//Re-enable the submit button
					$("input[type=submit]").attr("disabled", false);

                    // redirect the user if necessary
                    if (typeof data.responseJSON.redirect != 'undefined' && data.responseJSON.redirect.length > 0) {
                        window.location = data.responseJSON.redirect;
                    }

                    if (typeof data.responseJSON.validation != 'undefined') {
                        var $validator = $("#add-friend-form").validate();
                        try {
                            $validator.showErrors(data.responseJSON.validation);
                        }
                        catch (e) {
                            var errors = {"mobile": data.error.message};
                            $validator.showErrors(errors);
                        }
                    }
                },
                success: function(data){
                    //success

                    //Hack to fix wierd modal overlay issue with this bootstrap version
                    $('.modal').on('shown.bs.modal', function() {
                      //Make sure the modal and backdrop are siblings (changes the DOM)
                      $(this).before($('.modal-backdrop'));
                      //Make sure the z-index is higher than the backdrop
                      $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
                    });

                    $('#add-friend-thank').modal({
                        backdrop: 'static',
                        show: true
                    });
                    $("input[type=submit]").attr("disabled", false);
                },
                dataType: "json"
            });
        },
        ignore: ''
    });

    $('form#spin-again-form').on('submit', function(e) {
        e.preventDefault();

		//Disable the submit button
		$("input[type=submit]").attr("disabled", true);

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: $(this).attr('action'),
            error: function(data){

				//Re-enable the submit button
				$("input[type=submit]").attr("disabled", false);

				// handle redirects
                if (typeof data.responseJSON != 'undefined' && typeof data.responseJSON.redirect != 'undefined' && data.responseJSON.redirect.length > 0) {
                    window.location = data.responseJSON.redirect;
                    return;
                }

                if (typeof data.responseJSON.validation != 'undefined') {
                    // show validation errors
                    var $validator = $("#entry-form").validate();
                    try {
                        $validator.showErrors(data.responseJSON.validation);
                    }
                    catch (e) {
                        var errors = {"mobile": data.error.message};
                        $validator.showErrors(errors);
                    }
                }
            },
            success: function(data){
                // success
                // spin wheel
                if (typeof data.prize != 'undefined' && data.prize.wheelIndex != 'undefined') {
                    var wheelIndex = data.prize.wheelIndex;
                    if (Number(wheelIndex) === wheelIndex && wheelIndex % 1 === 0) {
                        startSpin(wheelIndex, function() {
                            //console.log(data);
                            setTimeout(function() {
                                var location = '/win';

                                if (typeof data.redirect != 'undefined' && data.redirect.length > 0) {
                                    location = data.redirect;
                                }
                                if (typeof data.prize != 'undefined' && typeof data.prize.slug != 'undefined') {
                                    location += '?prize=' + data.prize.slug;
                                }

                                window.location = location;
                            }, 1500);
                        });
                    }
                }
            },
            dataType: "json"
        });
    });

   
});
