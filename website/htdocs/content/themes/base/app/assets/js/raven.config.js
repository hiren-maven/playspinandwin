Raven.config(ravenKey).install();
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
    Raven.captureMessage(errorMsg, {tags: {
        file: url,
        line: lineNumber,
        column: column,
    }});
}