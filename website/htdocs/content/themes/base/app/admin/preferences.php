<?php
use Model\PrizeModel;

$comp = array_keys(PrizeModel::prizeIndexes('comp'));
$compArray = array_combine($comp, $comp);

$vouchers = array_keys(PrizeModel::prizeIndexes('voucher'));
$vouchersArray = array_combine($vouchers, $vouchers);

$site_settings = Page::make('site_settings', 'Site Preferences')->set(array(
    'capability'    => 'manage_options', // Default capability
    'menu_icon'		=> 'admin-site', // An URL to an image asset
    'position'      => 58, // Position of the page in the admin
    'tabs'          => false // Make sections as tabs or not
));

$sections[] = Section::make('settings', 'Settings');

$site_settings->addSections($sections);

$settings['settings'] = array(
    Field::select('hard_prize', array(
        array_merge(array(''), array('-- Competitions-- '), $compArray, array('-- Vouchers --'), $vouchersArray)
    ), false, array(
        'title' => 'Set a prize to land on',
        'info'      => 'All wheel spins will land on this prize so long as it is set.',
        'default'   => ''
    )),
    Field::text('facebook_app_id',array(
        'title'     => 'Facebook APP ID',
        'info'      => 'Facebook application ID',
    )),
    Field::media('facebook_share_image_upload',array(
        'title'     => 'Facebook Share Image Upload',
        'info'      => 'Used in the meta tags, must be 1200 x 630 pixels',
    )),
    Field::text('facebook_share_image',array(
        'title'     => 'Facebook Share Image URL',
        'info'      => 'Used in the meta tags',
    )),
    Field::text('facebook_share_image_type',array(
        'title'     => 'Facebook Share Image Type',
        'info'      => 'Used in the meta tags, either image/jpeg, image/gif or image/png',
    )),
    Field::date('competition_start', array(
        'title'     => 'Competition Start Date',
        'info'      => 'Used for weighing voucher availability'
    )),
    Field::date('competition_end', array(
        'title'     => 'Competition End Date',
        'info'      => 'Used for weighing voucher availability'
    )),
    Field::text('twitter',array(
        'title'     => 'Twitter',
        'info'      => 'Twitter profile URL',
    )),
    Field::text('facebook',array(
        'title'     => 'Facebook',
        'info'      => 'Facebook profile URL',
    )),
    Field::text('youtube',array(
        'title'     => 'YouTube',
        'info'      => 'YouTube profile URL',
    )),
    Field::text('instagram',array(
        'title'     => 'Instagram',
        'info'      => 'Instagram profile URL',
    )),
    Field::text('ga_code',array(
        'title'     => 'Google Analytics',
        'info'      => 'Google analytics tracking code',
    )),
    Field::text('hotjar_code',array(
        'title'     => 'Hotjar',
        'info'      => 'Hotjar tracking code',
    )),
    Field::text('sentry_dsn',array(
        'title'     => 'Sentry DSN'
    )),
    Field::select('feature_phone', array(
        array(
            'no'  => 'No',
            'yes'   => 'Yes'
        )
    ), false, array(
        'title' => 'Feature Phone enabled',
        'info'      => 'Enable separate templates for feature phones.',
        'default'   => 'no'
    )),
    Field::media('terms_conditions',array(
        'title'     => 'Terms & conditions',
        'info'      => 'Upload a T&C\'s document for the site',
    )),
    Field::text('terms_conditions_url',array(
        'title'     => 'Terms & Conditions Media Link',
        'info'      => 'Enter the URL of the document uploaded above',
    )),
    Field::media('privacy_policy',array(
        'title'     => 'Privacy Policy',
        'info'      => 'Privacy policy document used for the site',
    ))
);

$site_settings->addSettings($settings);
