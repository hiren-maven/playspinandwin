<?php

$featurePhone = Option::get('site_settings', 'feature_phone');
$scriptsAdded = false;
if($featurePhone === 'yes'){
	$detect = new Mobile_Detect;
	if ($detect->isMobile() && $detect->mobileGrade() === "C" ) {
		//Feature phone scripts

		$scriptsAdded = true;
	}
}
elseif($scriptsAdded === false) {

	// Scripts
	Asset::add('jquery', 'js/vendor/jquery-1.11.0.min.js', null, '1.0', true);
	Asset::add('raven', 'js/vendor/raven.min.js', array('jquery'), '1.0', true);
	Asset::add('raven-config', 'js/raven.config.js', array('jquery'), '1.0', true);

	// vendor Scripts
	Asset::add('bootstrap', 'js/vendor/bootstrap.min.js', array('jquery'), '1.0', true);
	// Asset::add('fbconnect', 'js/vendor/fbconnect.min.js', array('jquery'), '1.0', true);
	Asset::add('owlcarousel', 'js/vendor/owl.carousel.min.js', array('jquery'), '1.0', true);

	Asset::add('multiselect', 'js/vendor/bootstrap-multiselect.js', array(), '1.0', true);

	// Main Script
	Asset::add('main', 'js/main.js', array('jquery'), '2.0', true);
	Asset::add('jmigrate', 'js/vendor/jmigrate.min.js', array('jquery'), '1.0', true);

	// Spin & Win Script
	Asset::add('spin-general-js', 'js/scripts.js', array('jquery'), '1.3', true);

	if (
		strpos(strtolower($_SERVER['REQUEST_URI']),'/win') !== 0 &&
		strpos(strtolower($_SERVER['REQUEST_URI']),'/sorry') !== 0
	) {
		// don't load the spin wheel scripts on the winnings page or the sorry page
		Asset::add('spin-winwheel-js', 'js/winwheel_1.2.js', array('jquery'), '1.2.1', true);
		Asset::add('spin-winwheel-start-js', 'js/start_winwheel.js', array('spin-winwheel-js'), '1.2', true);
	}

	Asset::add('screwDefaultButtons', 'js/vendor/jquery.screwdefaultbuttonsV2.min.js', array('jquery'), '1.2.1', true);


}
