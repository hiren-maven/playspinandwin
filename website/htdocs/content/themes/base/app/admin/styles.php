<?php

$featurePhone = Option::get('site_settings', 'feature_phone');
$stylesAdded = false;

/**
 FEATURE PHONE CSS
 **/
if($featurePhone === 'yes'){
    $detect = new Mobile_Detect;
    if ($detect->isMobile() && $detect->mobileGrade() === "C" ) {
        // Main Sylesheet
        //Asset::add('main-css', 'css/feature_main.css', false, '1.0', 'all');
        $stylesAdded = true;
    }
}
/**
 DESKTOP TABLET & SMART PHONE CSS
 **/
elseif($stylesAdded === false) {

    // Stylesheets
    Asset::add('bootstrap-css', 'css/bootstrap.min.css', array(), '1.0');
    Asset::add('spin-css', 'css/spin-win.css', array('bootstrap-css'), '1.0');
    Asset::add('owl-carousel-css', 'css/vendor/owl.carousel.css', false, '1.0', 'all');
    Asset::add('bootstrap-theme', 'css/vendor/bootstrap-theme.css', false, '1.0', 'all');
    Asset::add('bootstrap-multiselect', 'css/bootstrap-multiselect.css', false, '1.0', 'all');

    Asset::add('responsive', 'css/responsive.css', array(), '1.0');

    Asset::add('font-awesome', 'css/font-awesome.min.css', false, '1.0', 'all');

    // Fonts

    // Main Sylesheet
    //Asset::add('main-css', 'css/main.css', false, '2.0', 'all');

}
