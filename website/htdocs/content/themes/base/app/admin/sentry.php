<?php
/**
 * Setup sentry in order to catch all errors
 */
//DSN Example: https://8647fb5936474db58bf30f3a7e8a7285:9f9fb33e157c4b6fada987c738af5f1b@app.getsentry.com/51107

$sentryDsn = \Option::get('site_settings', 'sentry_dsn');
if(!empty($sentryDsn)){
    $dsnParts = parse_url($sentryDsn);
    $ravenJsKey = "{$dsnParts['scheme']}://{$dsnParts['user']}@{$dsnParts['host']}{$dsnParts['path']}";
    \View::share('ravenKey', $ravenJsKey);
    $client = new Raven_Client($sentryDsn, array(
        'tags' => array(
            'php_version' => phpversion(),
            'WordPress_version' => get_bloginfo( 'version', 'raw' ),
            'OS_version' => PHP_OS,
        )
    ));

    // Install error handlers and shutdown function to catch fatal errors
    $error_handler = new Raven_ErrorHandler($client);
    $error_handler->registerExceptionHandler();
    $error_handler->registerErrorHandler();
    $error_handler->registerShutdownFunction();
}