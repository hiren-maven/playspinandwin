<?php
// A custom condition function.
function is_api_signup()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/api/signup') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_api_spin()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/api/spin') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_api_add_friend()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/api/addfriend') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_api_social_share()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/api/socialshare') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_api_download_auth()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/api/downloadauth') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_spin()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/spin') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_spin_win()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/win') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_sorry()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/sorry') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_data_dump()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/data-dump') === 0){
        return true;
    }
    return false;
}

// A custom condition function.
function is_download_data()
{
    if(strpos(strtolower($_SERVER['REQUEST_URI']),'/download-data') === 0){
        return true;
    }
    return false;
}


// Then add the conditional function to the Route class.
add_filter('themosisRouteConditions', function($conds)
{
    $conds['api/signup'] = 'is_api_signup';
    $conds['api/spin'] = 'is_api_spin';
    $conds['api/addFriend'] = 'is_api_add_friend';
    $conds['api/socialShare'] = 'is_api_social_share';
    $conds['api/downloadAuth'] = 'is_api_download_auth';
    $conds['win'] = 'is_spin_win';
    $conds['spin'] = 'is_spin';
    $conds['sorry'] = 'is_sorry';
    $conds['data-dump'] = 'is_data_dump';
    $conds['download-data'] = 'is_download_data';
    return $conds; // Always return an associative array.
});