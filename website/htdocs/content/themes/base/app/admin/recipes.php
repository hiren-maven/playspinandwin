<?php

/*-----------------------------------------------------------------------*/
// Custom post type - rcps-recipes
// Example of custom post type - edit as you require
/*-----------------------------------------------------------------------*/
$recipes = PostType::make('rcps-recipes', 'Recipes', 'Recipe')->set(array(

    'public'    => true,
    'rewrite'   => array(
        'slug'      => 'recipes'
    ),
    'supports'  => array('title', 'excerpt'),
    'menu_icon' => 'dashicons-carrot',
    'menu_position'      => 5

));

/*-----------------------------------------------------------------------*/
// Recipe Meta
/*-----------------------------------------------------------------------*/
$recipe_meta = Metabox::make('Recipe Quicklook', $recipes->getSlug())->set(array(

    Field::text('prep_time', array(
        'title' => 'Prep Time',
        'type'  => 'number'
    )),
    
    Field::text('cook_time', array(
        'title' => 'Cooking Time',
        'type'  => 'number'
    )),

    Field::text('ingredient_no', array(
        'title' => 'Number of Ingredients',
    )),

    Field::text('serves', array(
        'title' => 'Serves',
    )),

));

$recipe_meta1 = Metabox::make('Recipe Details', $recipes->getSlug())->set(array(

    Field::media('recipe_image', array(
        'title' => 'Recipe image',
        'info'  => 'Insert high res image. backend will create necessary sizes'
    )),

     Field::editor('ingredients', array(
        'title' => 'Ingredients',
        'media_buttons' => false,
    )),

    Field::editor('instructions', array(
        'title' => 'Instructions',
        'media_buttons' => false,
    )),
    
    Field::media('recipe_download', array(
        'title' => 'Recipe download',
        'info'  => 'Upload PDF of recipe here for download'
    )),

));
