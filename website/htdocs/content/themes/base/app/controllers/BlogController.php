<?php
namespace Controller;

class BlogController extends BaseController
{

    public function __construct(){

        $this->template_group = $this->setViewFolder();

    }

    public function archive()
    {
        return \View::make(($this->template_group).'.blog.blog', array('class' => 'blog'));
    }

    public function single($post)
    {
        return \View::make(($this->template_group).'.blog.single', array('class' => 'blog-single'));
    }
}
