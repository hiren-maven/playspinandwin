<?php

namespace Controller;

use Model\BaseModel;
use \Model\EntrantModel;
use \Model\PrizeModel;
use \Model\FriendModel;

class ApiController extends BaseController{
    private $success = true;
    private $data = NULL;

    public function __construct(){

        if (strtolower($_SERVER['REQUEST_METHOD']) === 'post') {
            $this->data = $this->getContent();
        }
    }

    /**
     * Handle Entrant signup via POST request.
     */
    public function signup()
    {
        $this->isPost();

        $entrant = new EntrantModel;
        $friend = new FriendModel;


        // apparently there can be issues sending requests with any attributes named 'name'
        // these can lead 404 responses from perfectly functioning endpoints
        // so had to change the request to use 'fullname' instead
        // now we're just changing it back
        if (isset($this->data['fullname'])) {
            $this->data['name'] = $this->data['fullname'];
            unset($this->data['fullname']);
        }



        // perform exists check now, if not exists perform signup, else check if allowed to spin
        if (isset($this->data['email'])) {
            $exists = EntrantModel::findByEmail($this->data['email']);

            // check for burner emails
            if ($this->isBurnerEmail($this->data['email'])) {
                $this->success = false;
                $this->outputData(['validation' => [
                    'email' => 'Burner emails are not allowed',
                ]], 400);
            }
        } else {
            $exists = null;
        }

        /* @var $exists EntrantModel|null */
        if ($exists === null) {
           if ($entrant->signup($this->data)) {
                // add entrant session
                $this->setEntrantSession($entrant);
                // spin the wheel and send the prize and entrant details
                $prize = $this->performSpin($entrant);
                $entrant = EntrantModel::where('id', '=', $entrant->id)->first();
                $this->outputData(['exists' => false, 'entrant' => $entrant, 'prize' => $prize]);
            } else {
                $this->success = false;
                $this->outputData(['validation' => $entrant->errors], 400);
            }
        } else {
            // add entrant session
            $this->setEntrantSession($exists);
            if ($exists->canSpin()) {
                // spin the wheel and send the prize and entrant details
                $prize = $this->performSpin($exists);
                $entrant = EntrantModel::where('id', '=', $entrant->id)->first();

                $this->outputData(['exists' => true, 'entrant' => $entrant, 'prize' => $prize]);
            } else {
                // add session variable to track that user is out of spins from home, not from a share
                $this->setSessionVariable('cantSpinFromHome', true);
                // redirect to no more spins page
                $this->success = false;
                $this->outputData(['redirect' => '/sorry'], 403);
            }
        }


    }

    /**
     * @param EntrantModel $entrant
     * @return array
     */
    public function performSpin($entrant)
    {
        // ensure entrant is still allowed to spin the wheel
        if (!$entrant->canSpin()) {
            $this->success = false;
            $this->outputData(['redirect' => '/sorry'], 403);
        }
        $entrant->decrementSpins();
        return PrizeModel::givePrize($entrant);
    }

    /**
     * Spin API endpoint.
     */
    public function spin()
    {
        $this->getEntrantFromSession();
        $prize = $this->performSpin($this->entrant);
        //$this->unsetSpinningAgain();
        $this->outputData(['exists' => true, 'entrant' => $this->entrant, 'prize' => $prize]);
    }

    /**
     * Add Friend API endpoint.
     */
    public function addFriend()
    {

        $this->isPost();
        $this->getEntrantFromSession();

        $validation = array();
        for ($i=1; $i < 6; $i++)
        {
            if (isset($this->data["friend_email_$i"]) && strlen($this->data["friend_email_$i"]) > 0)
            {

                if ($this->entrant->maxPendingFriends() == true) {
                    $validation["friend_email_$i"] = "You have reached the max pending invites!";
                    continue;
                }

                if (filter_var($this->data["friend_email_$i"], FILTER_VALIDATE_EMAIL) === false) {
                    $validation["friend_email_$i"] = $this->data["friend_email_$i"] . " is not a valid email address";
                    continue;
                }

                // check if email address is real
                //uncomment this line after development is done
                $isReal = BaseModel::emailExists($this->data["friend_email_$i"], 'help@playspinandwin.co.za');
                if ($isReal == 'invalid') {
                    $validation["friend_email_$i"] = $this->data["friend_email_$i"] . " does not exist";
                    continue;
                }

                // check for burner emails
                if ($this->isBurnerEmail($this->data["friend_email_$i"])) {
                    $this->success = false;
                    //Insert validation error for burner emails
                    $validation["friend_email_$i"] = 'Burner emails are not allowed';
                    continue;
                }

                $data = array(  "friend_name"=>$this->data["friend_name_$i"],
                                "friend_email"=>$this->data["friend_email_$i"]);


                if ($this->entrant->addFriend($data)) {
                    /*$this->unsetSessionVariable('cantSpinFromHome');
                    $this->setSpinningAgain('email');*/
                } else {
                    $this->success = false;
                    //Cant use the error message from the model, so highjack them here.
                    $validation["friend_email_$i"] = "Friend already added, or there was in error";
                }
            }
        }

        if (count($validation) > 0)
        {
            $this->outputData(array('validation'=>$validation), 400);
        }

        $this->outputData([]);
    }

    /**
     * Social Share endpoint.
     */
    public function socialShare()
    {
        $this->isPost();
        $this->getEntrantFromSession();

        if (isset($this->data['platform'])) {
            $this->unsetSessionVariable('cantSpinFromHome');

            if ($this->entrant->share($this->data['platform'])) {
                $this->setSpinningAgain($this->data['platform']);
                $this->entrant->incrementSpins();
                $this->outputData([]);
            } else {
                $this->success = false;
                $this->outputData(['validation' => $this->entrant->errors], 400);
            }
        } else {
            $this->success = false;
            $this->outputData(['message' => 'seriously?! no platform?'], 403);
        }
    }

    /**
     * Authenticates credentials to access the download
     */
    public function downloadAuth()
    {
        $this->isPost();
        $username = 'CNC';
        $password = 'eTUnDXXb9m8FrvD5';

        if (
            !isset($this->data['username']) || !isset($this->data['password']) ||
            ($this->data['username'] !== $username || $this->data['password'] !== $password)
        ) {
            wp_redirect('/data-dump');
            exit;
        } else {
            $this->setSessionVariable('downloadAuthorized', true);
            wp_redirect('/download-data');
            exit;
        }
    }

    /**
     * @inheritdoc
     */
    protected function getEntrantFromSession()
    {
        if (!parent::getEntrantFromSession()) {
            $this->success = false;
            $this->outputData(['redirect' => '/'], 401);
        }
    }

    /**
     * @param string $email
     * @return bool
     */
    protected function isBurnerEmail($email)
    {
        $emailDomain = substr($email, strpos($email, '@', 1) + 1);

        return in_array($emailDomain, $this->burnerEmails());
    }

    /**
     * @return array
     */
    protected function burnerEmails()
    {
        $emails = @require('content/themes/base/app/storage/data/burner-emails.php');

        if (empty($emails)) {
            $emails = [];
        }

        return $emails;
    }

    /**
     * Handle exceptions in a rest friendly way
     * @param \Exception $exception
     */
    private function handleError(\Exception $exception){
        $this->success = false;
        $error = array(
            "code" => $exception->getCode(),
            "message" => $exception->getMessage()
        );
        $this->outputData(compact('error'),400);
    }

    /**
     * Ensure the HTTP Method is POST
     */
    private function isPost(){
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            $this->success = false;
            $this->outputData(array("error" => array(
                "code" => 001,
                "message" => "HTTP Method must be POST"
            )),404);
        }
    }

    /**
     * Get the POST content in the correct format depending on the content type
     * @return array|mixed POST input data
     */
    private function getContent(){
        $data = array();
        $rawData = file_get_contents('php://input');
        $contentType = strtolower($_SERVER["CONTENT_TYPE"]);

        switch($contentType){
            case 'application/xml':
                $this->success = false;
                $this->outputData(array("error" => array(
                    "code" => 514,
                    "message" => "XML is not supported"
                )),404);
                break;
            case 'application/x-www-form-urlencoded':
                parse_str($rawData,$data);
                break;
            case "application/json":
            default:
                $data = json_decode($rawData,true);
                break;
        }
        return $data;
    }

    /**
     * Helper funciton to display the JSON result
     * @param array $data Data to display
     * @param int $resCode HTTP response code
     */
    private function outputData($data = array(),$resCode = 200){
        http_response_code($resCode);
        $success = array('success'=>$this->success);
        $jsonData = array_merge($success,$data);
        echo json_encode($jsonData);
        exit(0);
    }
}
