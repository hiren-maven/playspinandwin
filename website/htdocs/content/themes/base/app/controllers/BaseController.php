<?php
namespace Controller;

use \Model\EntrantModel;

class BaseController extends \Controller
{
    /**
     * @var null|EntrantModel
     */
    protected $entrant = null;

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout))
        {
            $this->layout = \View::make($this->layout);
        }
    }

    protected function setViewFolder(){

        $featurePhone = \Option::get('site_settings', 'feature_phone');
        if($featurePhone !== 'yes'){
            return 'desktop';
        }
        else
        {
            $detect = new \Mobile_Detect;
            if ($detect->isMobile() && $detect->mobileGrade() === "C" ) {
                return 'mobile';
            }
        }
        return 'desktop';
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    protected function setSessionVariable($name, $value)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['spinAndWin'][$name] = $value;
    }

    /**
     * @param string $name
     * @return bool|mixed
     */
    protected function getSessionVariable($name)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (!isset($_SESSION['spinAndWin'][$name])) {
            return false;
        }

        return $_SESSION['spinAndWin'][$name];
    }

    /**
     * @param string $name
     */
    protected function unsetSessionVariable($name)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (isset($_SESSION['spinAndWin'][$name])) {
            unset($_SESSION['spinAndWin'][$name]);
        }
    }

    /**
     * Set the current entrant session.
     *
     * @param EntrantModel $entrant
     */
    protected function setEntrantSession($entrant)
    {
        //echo '<pre>'; print_r($entrant); echo '</pre>';

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['spinAndWin'] = [
            'entrant_id' => $entrant->id,
            'entrant_email' => $entrant->email,
        ];
    }

    /**
     * Load up the entrant from the session data.
     * If no session data is available, redirect to the home page.
     *
     * @return bool|EntrantModel
     */
    protected function getEntrantFromSession()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        // ensure entrant session exists
        if (!isset($_SESSION['spinAndWin']['entrant_id'])) {
            return false;
        }

        // load up the entrant's details
        $this->entrant = EntrantModel::findById($_SESSION['spinAndWin']['entrant_id']);

        return $this->entrant;
    }

    /**
     * Set session variables to use as a flag for spinning again.
     *
     * @param string $shareType
     */
    protected function setSpinningAgain($shareType)
    {
       if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['spinAndWin']['isSpinningAgain'] = true;
        $_SESSION['spinAndWin']['sharedVia'] = $shareType;
    }

    /**
     * Remove spinning again flag.
     */
    protected function unsetSpinningAgain()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (isset($_SESSION['spinAndWin']['isSpinningAgain'])) {
            unset($_SESSION['spinAndWin']['isSpinningAgain']);
        }
        if (isset($_SESSION['spinAndWin']['sharedVia'])) {
            unset($_SESSION['spinAndWin']['sharedVia']);
        }
    }

    /**
     * Check a session variable to see if entrant is spinning again.
     */
    protected function isSpinningAgain()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        /*if (isset($_SESSION['spinAndWin']['isSpinningAgain']) && $_SESSION['spinAndWin']['isSpinningAgain']) {
            return true;
        } else {
            return false;
        }*/

        if ($this->getEntrantFromSession()){
            return $this->entrant->hasSpins();
        }
        else{
            return false;
        }
        //var_dump ($_SESSION);
        //Query entrant for outstanding spins.
    }

    /**
     * Check a session variable to see what the entrant shared via.
     *
     * @return string|null
     */
    protected function getSharedVia()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (isset($_SESSION['spinAndWin']['sharedVia'])) {
            return $_SESSION['spinAndWin']['sharedVia'];
        } else {
            return null;
        }
    }
}
