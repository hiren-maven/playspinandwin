<?php

namespace Controller;

use Model\CompetitionModel;
use Model\EntrantModel;
use Model\FriendModel;
use Model\VoucherModel;

class SpinController extends BaseController
{
    public function __construct()
    {
        $this->template_group = $this->setViewFolder();
    }

    public function home()
    {
        $transaction_id = \Input::get('transaction_id', false);

        if ($transaction_id !== false) {
            $this->setHasOffersTxSession(htmlentities($transaction_id, ENT_QUOTES, 'UTF-8', false));
        }

        $params = [
            'class' => 'home',
        ];

        //This needs to be done with a query ....
        //How many firends have entered for which I have not spun ... field on the entrant detail?
        //The key is in the CanSpin parameter.
        //Number of outstanding friends - canspin value = potential spins
        //When friends enters can_spin += 1
        //When spin can_spoin -= 1

        if ($this->isSpinningAgain() && $this->getEntrantFromSession()) {
            $params['isSpinningAgain'] = true;
            $params['sharedVia'] = $this->getSharedVia();
            $params['fbLimitReached'] = $this->entrant->hasShared('facebook');
            $params['twitterLimitReached'] = $this->entrant->hasShared('twitter');
        } else {
            $params['isSpinningAgain'] = false;
        }

        // adding scripts here, as I cannot check for home route in admin/scripts.php
        \Asset::add('has-offers-config', 'js/has_offers.js', array(), '0.1', true);
        \Asset::add('has-offers', 'http://media.go2speed.org/assets/js/dl.js', array('has-offers-config'), '1.0', true);

        //perform exists check for entrant comes from result email
        if(!empty($_SESSION)){
            $entrant_id = $_SESSION['spinAndWin']['entrant_email'];
            $isentered = FriendModel::countOfEntrant($entrant_id);
            if ($isentered > 0) {
               //if entrant is already in database than redirect to sorry page  
               $rediect_url = home_url().'/sorry';
               header('Location:'.$rediect_url);
            } else {
                //if entrant is not in in database than redirect to home page
                return \View::make(($this->template_group).'.pages.spin.home', $params);    
            }    
        }
        return \View::make(($this->template_group).'.pages.spin.home', $params);  
    }

    public function spin()
    {
        $this->entrantSessionId();

        return \View::make(($this->template_group).'.pages.spin.spinner', array('class' => 'home'));
    }

    public function sorry()
    {
        /* @var EntrantModel $entrant */
        $entrant = EntrantModel::where('id', '=', $this->entrantSessionId())->with('vouchers', 'entries')->first();

        // check if user was directed here from home
        $fromHome = $this->getSessionVariable('cantSpinFromHome');

        // template parameters
        $params = [
            'class' => 'home',
            'fbLimitReached' => $entrant->hasShared('facebook'),
            'twitterLimitReached' => $entrant->hasShared('twitter'),
            'fromHome' => $fromHome,
        ];

        return \View::make(($this->template_group).'.pages.spin.sorry', $params);
    }

    public function win()
    {
       

        if ($transaction_id = $this->getHasOffersTxSession())
        {
            //Do a quick call to hasOffers conversion.
            file_get_contents("http://t.clicktracker.co.za/aff_lsr?offer_id=37&transaction_id=$transaction_id");
        }

        /* @var EntrantModel $entrant */
        $entrant = EntrantModel::where('id', '=', $this->entrantSessionId())->with('vouchers', 'entries')->first();

        // template parameters
        $params = [
            'class' => 'home',
            'fbLimitReached' => $entrant->hasShared('facebook'),
            'twitterLimitReached' => $entrant->hasShared('twitter'),
        ];

        // voucher descriptions
    
        $voucherDescriptions = [
            'OneDayOnly'=>[
                '100'=>'A R100 VOUCHER FROM ONEDAYONLY',
            ],
            'CyberCellar' => [
                'R50 off R250' => 'A R50 VOUCHER FROM CYBERCELLAR',
                'R 50' => 'A R50 VOUCHER FROM CYBERCELLAR',
                'R 75' => 'A R75 VOUCHER FROM CYBERCELLAR',
                'R 100' => 'A R100 VOUCHER FROM CYBERCELLAR',
                'R 150' => 'A R150 VOUCHER FROM CYBERCELLAR',
                'R 200' => 'A R200 VOUCHER FROM CYBERCELLAR',
            ],
            'Ucook' => [
                'R100' => 'A R100 VOUCHER FROM UCOOK',
                'R300' => 'A R300 VOUCHER FROM UCOOK',
                'R500' => 'A R500 VOUCHER FROM UCOOK',
                'R1000' => 'A R1000 VOUCHER FROM UCOOK',
            ],
            'SweepSouth'=>[
                'R50'=>'A R50 VOUCHER FROM SWEEPSOUTH',
                'R100'=>'A R100 VOUCHER FROM SWEEPSOUTH',
                'R250'=>'A R250 VOUCHER FROM SWEEPSOUTH',
                'R1000'=>'A R1000 VOUCHER FROM SWEEPSOUTH',
            ],
            'Uber' => [
                '200' => 'A R200 VOUCHER FROM UBER',
            ],
        ];


        // get a prize description from the url
        if (isset($_GET['prize'])) {
            $prize = $_GET['prize'];
            $parts = explode('/', $prize);

            if (count($parts) == 2 && $parts[0] == 'comp') {
                // load competition
                $comp = CompetitionModel::findBySlug($parts[1]);

                $params['win_type'] = $comp->slug;

                // security checks
                if ($comp !== null) {
                    $won = $comp->details;
                }
            } elseif (count($parts) == 3 && $parts[0] == 'voucher') {
                // get type and subtype
                $type = $parts[1];
                $subtype = $parts[2];

                 $params['win_type'] = $type;

                // security checks
                if (
                    array_key_exists($type, $voucherDescriptions) &&
                    array_key_exists($subtype, $voucherDescriptions[$type])
                ) {
                    $won = $voucherDescriptions[$parts[1]][$parts[2]];
                }
            }
        }

        if (!isset($won)) {
            $entries = $entrant->entries;
            $vouchers = $entrant->vouchers;

            // let's see what they won
            if ($vouchers->count() > 0) {
                // won a voucher   
                foreach ($vouchers as $voucher) {
                    $won = $voucherDescriptions[$voucher->type][$voucher->subtype];
                    $params['voucher'] = $voucher;
                    break;
                }
            } elseif ($entries->count() > 0) {
                // won an entry into a competition
                foreach ($entries as $entry) {
                    $won = $entry->competition->details;
                    break;
                }
            }
        }

        if (!isset($won) || empty($won)) {
            // won nothing: send 'em back to the depths from whence they came!
            wp_redirect('/', 302);
            exit;
        }

        $params['won'] = $won;
        $params['available_spins'] = $entrant->available_spins;

        //echo '<pre>'; print_r($params);echo '</pre>';
        return \View::make(($this->template_group).'.pages.spin.win', $params);
    }

    public function dataAuth()
    {
        return \View::make(($this->template_group).'.pages.spin.data-dump', array());
    }

    public function downloadData()
    {
        // check if user is permitted to download this file
        if ($this->getSessionVariable('downloadAuthorized') !== true) {
            http_response_code(401);
            exit;
        }

        // remove permission for future requests
        $this->unsetSessionVariable('downloadAuthorized');

        // get entrants
        $entrants = EntrantModel::all();
        // set a filename
        $filename = 'Entrants_' . date('Y-m-d_H-i-s') . '.csv';

        // clean output buffer just in case
        ob_clean();

        // prepare output buffers
        ob_start();

        // output csv headers
        echo 'ID,Name,Email,"Contact No",Age,Gender,Interests,Location,Source,"Created Date","Last FB Share","Last Twitter Share"' . "\n";

        if ($entrants !== false) {
            // loop through entrants and output their details
            foreach ($entrants as $entrant) {
                /* @var $entrant EntrantModel */
                echo '"' . $entrant->id . '","' . $entrant->name . '","' . $entrant->email . '","' . $entrant->contactno . '","' . $entrant->age . '","' . $entrant->gender . '","' . $entrant->interests . '","' . $entrant->location . '","' . $entrant->source . '","' . $entrant->created_date . '","' . $entrant->last_fb_share . '","' . $entrant->last_twitter_share . '"' . "\n";
            }
        }

        // set file headers
        http_response_code(200);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $filename . '');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . ob_get_length());

        // flush output
        ob_end_flush();

        // kill the script
        exit;
    }

    protected function getHasOffersTxSession()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        // ensure entrant session exists
        if (isset($_SESSION['transaction_id'])) {
            return $_SESSION['transaction_id'];
        } else {
            return false;
        }
    }

    protected function setHasOffersTxSession($transaction_id)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $_SESSION['transaction_id'] = $transaction_id;
    }

    protected function entrantSessionId()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        // ensure entrant session exists
        if (isset($_SESSION['spinAndWin']['entrant_id'])) {
            return $_SESSION['spinAndWin']['entrant_id'];
        } else {
            wp_redirect('/', 302);
            exit;
        }
    }

}
