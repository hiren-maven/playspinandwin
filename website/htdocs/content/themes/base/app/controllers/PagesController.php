<?php

namespace Controller;
use \DrewM\MailChimp\MailChimp;

class PagesController extends BaseController{

    public function __construct(){

        $this->template_group = $this->setViewFolder();
    }

    public function home()
    {
    	return \View::make(($this->template_group).'.pages.page', array('class' => 'home'));
    }
    
    public function competition_end(){
    	$status = 'error';
    	if (isset($_POST['email']) && !!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
    	{    		
	    	$MailChimp1 = new MailChimp('910063992d7f06b9d4789824e65ed2c4-us13');
	        $list_id = 'f315ce0d60';

	        $result = $MailChimp1->post("lists/$list_id/members", [
	            'email_address' => $_POST['email'],
	            'status'        => 'subscribed'
	        ]);
	        $status = 'success';
    	}
    	return \View::make('desktop/pages/competitionend', compact('status'));
    }
}
