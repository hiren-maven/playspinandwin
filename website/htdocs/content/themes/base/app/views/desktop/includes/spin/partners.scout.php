<!--Partner Logos-->
<div class="head-logos">
	<div class="container">
		<div class="row">
			<ul class="head-logos-ul">
				<li><a href="http://compareguru.co.za/"><img src="<?php echo themosis_assets() . '/images/brands/compareguru-logo.png'; ?>" alt="Compare Guru"></a></li>
				<li><a href="http://www.cybercellar.com/"><img src="<?php echo themosis_assets() . '/images/brands/cybercellar-logo.png'; ?>" alt="Cyber Cellar"></a></li>
				<li><a href="http://www.pricecheck.co.za/"><img src="<?php echo themosis_assets() . '/images/brands/pricecheck-logo.png'; ?>" alt="Price Check"></a></li>
				<li><a href="http://www.ucook.co.za/"><img src="<?php echo themosis_assets() . '/images/brands/ucook-logo.png'; ?>" alt="U Cook"></a></li>
				<li><a href="http://www.spree.co.za/"><img src="<?php echo themosis_assets() . '/images/brands/spree-logo.png'; ?>" alt="Spree"></a></li>
				<li><a href="http://www.uber.com/"><img src="<?php echo themosis_assets() . '/images/brands/uber-logo.png'; ?>" alt="Uber"></a></li>
				<li><a href="http://www.onedayonly.co.za/"><img src="<?php echo themosis_assets() . '/images/brands/onedayonly-logo.png'; ?>" alt="One Day Only"></a></li>
				<li><a href="http://www.petheaven.co.za/"><img src="<?php echo themosis_assets() . '/images/brands/petheaven-logo.png'; ?>" alt="Petheaven"></a></li>
				<li><a href="https://sweepsouth.com/?utm_source=spinandwin2016&utm_medium=partnership&utm_campaign=spinandwin2016.voucher"><img src="<?php echo themosis_assets() . '/images/brands/sweepsouth-logo.png'; ?>" alt="Sweep South"></a></li>
			</ul>
		</div>
	</div>
</div>
<!--Partner Logos Ends--> 