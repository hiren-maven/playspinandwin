<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta property="og:url"                content="<?php echo getenv('WP_HOME'); ?>" />
	<meta property="og:title"              content="Spin and Win" />
	<meta property="og:type" content="website" />
	<meta property="og:description"        content="Spin the wheel to win instant prizes to the value of R400 000, or be entered into the draw to win BIG!" /> 
	<meta property="og:image"              content="<?php echo \Option::get('site_settings', 'facebook_share_image'); ?>" />
	<meta property="og:image:type"         content="<?php echo \Option::get('site_settings', 'facebook_share_image_type') ?: 'image/png'; ?>" />
    <meta property="og:image:width"        content="1200" />
    <meta property="og:image:height"       content="630" />
    <meta property="fb:app_id"             content="<?php echo \Option::get('site_settings', 'facebook_app_id'); ?>" />

		<!-- Hotjar Tracking Code for http://playspinandwin.co.za/ -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:192538,hjsv:5};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
	</script>


  <link rel="image_src" type="image/jpeg" href="img_path" />

	<title>Spin and Win</title>
	
	<script>
				window.twttr = (function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0],
					t = window.twttr || {};
				if (d.getElementById(id)) return t;
				js = d.createElement(s);
				js.id = id;
				js.src = "https://platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js, fjs);

				t._e = [];
				t.ready = function(f) {
					t._e.push(f);
				};

				return t;
			}(document, "script", "twitter-wjs"));

			twttr.ready(function (twttr) {
			    twttr.events.bind('tweet', function (intentEvent) {
				    setTimeout(function() {
						saveSocialShare('twitter');
				    }, 10000);
			    });
	    });
	</script>

	<?php $ga = \Option::get('site_settings', 'ga_code'); if (isset($ga) && !empty($ga)) { ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $ga; ?>', 'auto');
		ga('send', 'pageview');

	</script>
	<?php } ?>



	<?php wp_head(); ?>
</head>

<body>

<header>
<div class="main-header-bg min-height-bg-home">

<!-- CONTENT -->
@include('desktop.includes.spin.partners')
