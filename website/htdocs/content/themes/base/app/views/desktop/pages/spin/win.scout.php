<?php
/* @var string $won */
/* @var bool $fbLimitReached */
/* @var bool $twitterLimitReached */
/* @var string $win_type */
?>
@include('desktop.includes.spin.header')
<?php 
if(isset($_SESSION['spinAndWin']['entrant_email'])){
	$session_email = $_SESSION['spinAndWin']['entrant_email'];
	$track_link = "https://p.trackmytarget.com/?campaignID=wk0oqh&productID=4ps8tb&conversionType=lead&transactionID=".$session_email;
	?>
	<img src="<?php echo $track_link; ?>" width=1 height=1 border=0>
	<?php
}
?>
<!--Header Title-->
<div class="container">
<div class="row">
<div class="min-height-bg-ttl">
<h1 class="h1-main-ttl text-center"><img src="<?php echo themosis_assets() . '/images/spinwin-logo.png'; ?>" class="img-responsive" alt="Spin And Win"><br>
<?php if($win_type == 'Spree'){ ?>
<img src="<?php echo themosis_assets() . '/images/mailer/dynamic/tagline-spree.png'; ?>" class="img-responsive" alt="Spin And Win">
<?php }else{ ?>
<img src="<?php echo themosis_assets() . '/images/spinwin-tagline.png'; ?>" class="img-responsive" alt="Spin And Win">
<?php } ?>

<span class="tag-ttl"><?php echo $won; ?></span></h1>
<div class="col-md-5 banner-left-main">
<div class="banner-left text-center">

<?php if($win_type == 'PetHeaven') { ?>
	<img src="<?php echo themosis_assets().'/images/win_img/petheaven.png'; ?>" class="img-responsive" alt="petheaven">
<?php }else if($win_type == 'Spree'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/spree.png'; ?>" class="img-responsive" alt="Spree">
<?php }else if($win_type == 'OneDayOnlyADULTDRAW'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/odo-adult-draw.png'; ?>" class="img-responsive" alt="OneDayOnly Adult Draw">
<?php }else if($win_type == 'OneDayOnlyCHILDDRAW'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/odo-kids-draw.png'; ?>" class="img-responsive" alt="OneDayOnly Kids Draw">
<?php }else if($win_type == 'OneDayOnly'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/odo-voucher.png'; ?>" class="img-responsive" alt="VOUCHER">
<?php }else if($win_type == 'Uber'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/uber.png'; ?>" class="img-responsive" alt="VOUCHER">
<?php }else if($win_type == 'Ucook'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/ucook.png'; ?>" class="img-responsive" alt="VOUCHER">
<?php }else if($win_type == 'SweepSouth'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/sweepsouth.png'; ?>" class="img-responsive" alt="VOUCHER">			
<?php }else if($win_type == 'CyberCellar'){ ?>
	<img src="<?php echo themosis_assets().'/images/win_img/cybercellar.png'; ?>" class="img-responsive" alt="CyberCellar">
<?php } ?>

</div>
</div>
<div class="col-md-7 text-center banner-right">
<p class="banner-text">
<?php if ($available_spins > 0) { ?>
<form id="" method="get" action="/">
	<div class="form-wrapper">
		<input type="submit" value="Spend a Spin!" class="btn btn spin-now-button" />
	</div>
</form>
You have <?php echo $available_spins ?> spin(s) remaining!
<?php } ?>

Have a look in your inbox for more details.<br>Share the competition with your friends, by entering their email below<br>or sharing on social media, and get another spin!</p>

@include('desktop.includes.spin.add-friend')
@include('desktop.includes.spin.share', ['fbLimitReached' => $fbLimitReached, 'twitterLimitReached' => $twitterLimitReached])

</div>
</div>
</div>
</div>
<!--Header Title Ends-->
</div>
</header>
<section>
<div class="container">
<div class="row">
<div class="spin-wheel-content text-center">
<h4 class="h4-main">Up For Grabs</h4>
<div class="content-bottom">
<p>Fashion vouchers to the value of R50 000  |  R200 off your first Uber ride  |  Food &amp; wine vouchers to the value of R150 000  |  E-commerce vouchers to the value of R100 000  |  FREE Pet Food for a year to the value of R50 000  |  Additional product prizes valued at over R10 000</p>
</div>
<p class="terms-condition"><a href="<?php echo home_url().'/terms'; ?>">Terms &amp; Conditions</a>  | <a href="<?php echo home_url().'/help'; ?>">  Frequently Asked Questions</a></p>
</div>
</div>
</div>
</section>
@include('desktop.includes.spin.footer')