<form class="send-invites-form text-center" id="add-friend-form" method="post" action="/api/addFriend">
	<h2 class="h2-main text-center">or share with your friends via email?</h2>
	<div class="col-md-12 col-xs-12">
		<div class="row">
			<div class="form-group">
				<span>1</span> 
                <div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Full name" name="friend_name_1" />
				<label for="friend_name_1" generated="true" class="error"></label>
				</div>
                <div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Email" name="friend_email_1"/>
				<label for="friend_email_1" generated="true" class="error"></label>
                </div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="row">
			<div class="form-group"><span>2</span> 
                 <div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Full name" name="friend_name_2"/>
				<label for="friend_name_2" generated="true" class="error" style="display: none; width: 100%; text-align: right;"></label>
</div>
<div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Email" name="friend_email_2"/>
				<label for="friend_email_2" generated="true" class="error" style="display: none; width: 100%; text-align: right;"></label>
</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="row">
			<div class="form-group"><span>3</span> 
            <div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Full name" name="friend_name_3"/>
				<label for="friend_name_3" generated="true" class="error" style="display: none; width: 100%; text-align: right;"></label>
                </div>
                
<div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Email" name="friend_email_3"/>
				<label for="friend_email_3" generated="true" class="error" style="display: none; width: 100%; text-align: right;"></label>				
</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="row">
			<div class="form-group"><span>4</span> 

<div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Full name" name="friend_name_4"/>
				<label for="friend_name_4" generated="true" class="error" style="display: none; width: 100%; text-align: right;"></label>
</div>
<div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Email" name="friend_email_4"/>
				<label for="friend_email_4" generated="true" class="error" style="display: none; width: 100%; text-align: right;"></label>
</div>

			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="row">
			<div class="form-group"><span>5</span> 
<div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Full name" name="friend_name_5"/>
				<label for="friend_name_5" generated="true" class="error" style="display: none;"></label>
</div>
<div class="invite-group">
				<input class="form-control" type="text" placeholder="Friends Email" name="friend_email_5"/>
				<label for="friend_email_5" generated="true" class="error" style="display: none;"></label>
</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="row">
			<div class="form-group"><button class="btn send-invites-btn">Send Invites</button></div>
		</div>
	</div>
	
</form>
<!-- Modal -->
<div class="modal fade" id="add-friend-thank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h4 class="modal-title" id="myModalLabel">Thank you</h4>
	  </div>
	  <div class="modal-body">
		We have invited your friends. For each of your friends that enter you will earn yourself another spin! We will notify you by email when your spin is ready.
	  </div>
	  <div class="modal-footer">
		<a href="/"><button type="button" class="btn btn-success">Back home</button></a>
		<button type="button" class="btn btn-primary" data-dismiss="modal">Invite more</button>
	  </div>
	</div>
  </div>
</div>
