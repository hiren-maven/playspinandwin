<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

    <title><?php wp_title(''); ?></title>

    <!-- Hotjar Tracking Code for http://playspinandwin.co.za/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:192538,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
  
    <!--
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="canonical" href="<site url>" />

    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="fb:admins" content="<fb_id>" />
    <meta property="og:site_name" content="<site_name>"/>
    <meta property="og:url" content="<current_url>"/>
    <meta property="og:title" content="<page_title>"/>
    <meta property="og:description" content="<page desc>"/>
    <meta property="og:image" content=""/>
    -->
<?php wp_head(); ?>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{ themosis_assets() }}/js/3rdparty/html5shiv.min.js"></script>
      <script src="{{ themosis_assets() }}/js/3rdparty/respond.min.js"></script>
    <![endif]-->

    <?php
        $iconUrl = themosis_assets().'/images/favicon.png';
        $hotjarId = Option::get('site_settings', 'hotjar_code');
     ?>

    <link rel="icon" type="image/png" href="{{ $iconUrl }}" />

    <?php if(!empty($hotjarId)): ?>
    <!-- Hotjar Tracking Code for wearemonsters.co.za -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:{{$hotjarId}},hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <?php endif; ?>

</head>
<body class="{{{ $class or 'default' }}}">

@include('desktop.includes.navigation')
