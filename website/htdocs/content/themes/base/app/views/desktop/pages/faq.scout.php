@include('desktop.includes.spin.header')

<!--Header Title-->
<div class="container">
<div class="row">
<div class="header-title-bg min-padding">
<h1 class="h1-main-ttl text-center"><img src="<?php echo themosis_assets() . '/images/spinwin-logo.png'; ?>" class="img-responsive" alt="Spin And Win"><span>Frequently Asked Questions</span></h1>
</div>
</div>
</div>
<!--Header Title Ends-->
</div>
</header>
<section>
<div class="container">
<div class="row">
<div class="faqs-section">
<a href="<?php echo home_url(); ?>" class="back-home">Back Home</a>
<div class="que">How does it work?</div>
<p class="ans">The following are steps indicating the journey you will go on when entering the Spin&amp;Win:</p>
<ul class="faq-list">
<li><span>1.</span> <p>You will land on the Spin&amp;Win platform (<a href="#">www.playspinandwin.co.za</a>) from clicking on a link in a newsletter / banner/ social media post</p></li>
<li><span>2.</span> <p>You will be asked to enter your:</p>
<ul>
  <li>a.  Name</li>
  <li>b.  Email</li>
  <li>c.  Contact number</li>
  <li>d.  Age</li>
  <li>e.  Gender</li>
  <li>f.  Location</li>
  <li>g.  Interests</li>
    </ul>
    </li>
<li><span>3.</span> <p>You will then click the ‘Spin Now’ button</p></li>
<li><span>4.</span> <p>The wheel will spin and land on a specific slice and you will be navigated to the associated result page containing the prize information. You will also get emailed a result email, at the same time, with more details.</p></li>
<li><span>5.</span> <p>You can then leave the platform <strong>OR</strong></p></li>
<li><span>6.</span> <p>Enter other friends’ names and emails and spin again. Refer back to step 4 <strong>OR</strong></p></li>
<li><span>7.</span> <p>Share on Facebook / Twitter and spin again (only one Facebook share or Tweet is allowed per day). Refer back to step 4</p></li>
</ul>
<div class="que">Why is my email not being accepted?</div>
<p class="ans">The platform is set up in such a way so as not to accept any spam domains. If you are inserting an authentic email address that is not being accepted, please contact <a href="mailto:help@spinandwin.co.za">help@playspinandwin.co.za</a></p>
<div class="que">How do I use the vouchers?</div>
<p class="faq-text">Redemption instructions for all vouchers are included on the voucher design in the bottom, middle section. All sponsor URLs are included at the bottom of the result emails as well.</p> 
<p class="faq-text">The basic way to redeem these vouchers is by entering the voucher code provided in the center of the voucher design into field provided in brand’s checkout window.</p>
<p>Should you run into any issues in this regard, please email <a href="mailto:help@spinandwin.co.za">help@playspinandwin.co.za</a></p>
<div class="que">Who are the brands taking part?</div>
<p>Please see below a brief company description of each brand taking part in the Spin&amp;Win:</p>
<ul>
<li>PriceCheck (<a href="http://www.pricecheck.co.za">www.pricecheck.co.za</a>) - Africa’s largest product &amp; financial services platform.</li>
<li>CompareGuru (<a href="http://compareguru.co.za/">www.compareguru.co.za</a>) - Africa’s leading service comparison Website.</li>
<li>Spree (<a href="http://www.spree.co.za/">www.spree.co.za</a>) - Spree.co.za, part of Media24's Ecommerce division, is one of South Africa's leading online fashion retailers and the first online shop to offer a magazine-styled shopping experience.</li>
<li>Uber (<a href="http://compareguru.co.za/">www.compareguru.co.za</a>) - International on-demand taxi service.</li>
<li>OneDayOnly (<a href="http://www.onedayonly.co.za/">www.onedayonly.co.za</a>) - At OneDayOnly we do our best to provide as much information about ourselves and our daily sales right here on the site.</li>
<li>CyberCellar (<a href="http://www.cybercellar.com">www.cybercellar.com</a>) - E-commerce store specialising in the sale of premium wines, beers, liqueurs, and wine accessories.</li>
<li>Ucook (<a href="http://www.ucook.co.za/">www.ucook.co.za</a>) - Great recipes with fresh ingredients delivered to your door weekly.</li>
<li>PetHeaven (<a href="http://www.petheaven.co.za/">www.petheaven.co.za</a>) - We believe that getting your dog food, cat food or other pet accessories should be easier. That's why we created Pet Heaven, an online pet food store.</li>
<li>SweepSouth (<a href="https://sweepsouth.com/?utm_source=spinandwin2016&utm_medium=partnership&utm_campaign=spinandwin2016.voucher">www.sweepsouth.com</a>) - We provide a convenient way to book home cleaning services online from a phone, tablet or computer, connecting clients to cleaners in minutes. By bringing technology into the process, we aim to modernise the domestic cleaning services industry.</li>
</ul>
<div class="que">Why am I not allowed to share on social media?</div>
<p class="faq-text">If you are unable to share on Facebook or Twitter, it is because you have already reached your daily limit of one Facebook share or Tweet. Try again tomorrow and you should be able to without a problem! Alternatively, enter a friend’s name and email to share with them and get another spin.</p> 
<p>If you are still experiencing issues, please contact <a href="mailto:help@spinandwin.co.za">help@spinandwin.co.za.</a></p>
<a href="<?php echo home_url(); ?>" class="back-home">Back Home</a>
</div>

</div>
</div>
</section>

@include('desktop.includes.spin.footer')
