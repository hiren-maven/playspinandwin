<html>
<head>
<title>Spin &amp; Win</title>
<style>

@font-face {
  font-family: 'Lato';
  src: url('/content/themes/base/app/assets/fonts/Lato-Regular.eot'); /* IE9 Compat Modes */
  src: url('/content/themes/base/app/assets/fonts/Lato-Regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/content/themes/base/app/assets/fonts/Lato-Regular.woff2') format('woff2'), /* Modern Browsers */
       url('/content/themes/base/app/assets/fonts/Lato-Regular.woff') format('woff'), /* Modern Browsers */
       url('/content/themes/base/app/assets/fonts/Lato-Regular.ttf') format('truetype');
  font-style: normal;
  font-weight: normal;
  text-rendering: optimizeLegibility;
}
.autoResizeImage {
    max-width: 100%;
    height: auto;
    width: 100%;
}
.form-container {
    display: inline-block;
    width: 31%;
    margin: 0 auto !important;
    position: relative;
}
form input{
    font-family: Lato;
}
form input[type="email"] {
    width: 65%;
    height: 38px;
    padding-left: 10px;
    transition: all 0.4s linear;
}
form input[type="submit"] {
    height: 40px;
    margin-left: 13px;
    background-color: #fe9900;
    border: none;
    padding: 8px;
    color: #fff;
    font-size: 15px;
}
form input[type="email"]::-webkit-input-placeholder {
    font-family: 'Lato';
    font-size: 17px;
    font-weight: 700;
    color: #999999;
}

form input[type="email"]:-ms-input-placeholder {
    font-family: 'Lato';
    font-size: 17px;
    font-weight: 700;
    color: #999999;
}

form input[type="email"]:-moz-placeholder {
    font-family: 'Lato';
    font-size: 17px;
    font-weight: 700;
    color: #999999;
}

form input[type="email"]::-moz-placeholder {
    font-family: 'Lato';
    font-size: 17px;
    font-weight: 700;
    color: #999999;
}

@media (max-width: 1170px) {
    form input[type="email"] {
        width: 51%;
        height: 25px;
        padding-left: 10px;
    }

    form input[type="submit"] {
        height: 28px;
        padding: 7px;
        font-size: 13px;
    }



    form input[type="email"]::-webkit-input-placeholder {
        font-family: 'Lato';
        font-size: 13px !important;
        font-weight: 700;
        color: #999999;
    }
    form input[type="email"]:-ms-input-placeholder {
        font-family: 'Lato';
        font-size: 13px !important;
        font-weight: 700;
        color: #999999;
    }
    form input[type="email"]:-moz-placeholder {
        font-family: 'Lato';
        font-size: 13px !important;
        font-weight: 700;
        color: #999999;
    }
    form input[type="email"]::-moz-placeholder {
        font-family: 'Lato';
        font-size: 13px !important;
        font-weight: 700;
        color: #999999;
    }
}

@media (max-width: 800px) {
    form input[type="email"] {
        width: 100%;
        height: 25px;
        padding-left: 10px;
        margin: 9px 0;
    }
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <img class="autoResizeImage" src="content/themes/base/app/assets/images/competition_winners_top.png"  alt=""></td>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <div class="form-container">
                <form method="post" action="">
                    <input type="email" name="email" placeholder="Your e-mail" required/>
                    <input type="submit" value="Let Me Know!"/>
                    @if (isset($status) && $status == 'success') 
                        <p style="color: green;">Your email was sucessfully added.</p>
                    @endif
                </form>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <img class="autoResizeImage" src="content/themes/base/app/assets/images/competition_winners_bottom.png"  alt=""></td>
        </td>
    </tr>
</table>
</body>
</html>