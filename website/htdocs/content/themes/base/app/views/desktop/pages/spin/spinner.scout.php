@include('desktop.includes.spin.header')

	<div class="spinning-wheel-wrapper">
	<canvas class="the_canvas" id="myDrawingCanvas" width="503" height="503" data-wheel-image="<?php /*echo themosis_assets() . '/images/spin.png';*/ ?>" data-spin-on="<?php /*echo themosis_assets() . '/images/spin_on.png';*/ ?>" data-spin-off="<?php /*echo themosis_assets() . '/images/spin_off.png';*/ ?>">
		<p class="noCanvasMsg" align="center">Sorry, your browser doesn't support canvas.<br />Please try another.</p>
	</canvas>
	</div>

@include('desktop.includes.spin.footer')