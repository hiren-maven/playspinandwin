<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Project name</a>
    </div>
    <div id="navbar" class="desktop_menu collapse navbar-collapse">
      <?php
      wp_nav_menu(array(
        'theme_location' => 'header-nav',
        'container' => false,
        'menu_class'  => 'nav navbar-nav navbar-right'
      ));
      ?>
    </div>
  </div>
</nav>
