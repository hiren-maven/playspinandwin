<?php
/* @var bool $fbLimitReached */
/* @var bool $twitterLimitReached */
/* @var bool $fromHome */
?>
@include('desktop.includes.spin.header')

<!--Header Title-->
<div class="container">
<div class="row">
<div class="min-height-bg-ttl">
<div class="header-title-bg min-height-bg-ttl">
<h1 class="h1-main-ttl text-center"><img src="<?php echo themosis_assets() . '/images/spinwin-logo.png'; ?>" class="img-responsive" alt="Spin And Win">
<span>
<?php 
if ($fromHome && $fbLimitReached && $twitterLimitReached): 
	$setstyle = "margin-top:-65px";
?>
	Sorry, you have reached your Facebook & Twitter share limit for the day.
<?php elseif ($fromHome && $fbLimitReached): 
	$setstyle = "";
?>
	Sorry, you have reached your Facebook shares for the day.
	<br>Why not share on Twitter?
<?php elseif ($fromHome && $twitterLimitReached): 
	$setstyle = "";
?>
	Sorry, you have reached your Twitter shares for the day.
	<br>Why not share on Facebook?
<?php else: 
	$setstyle = "";
?>
	Sorry, you have reached your entry limit.
	<br>Why not share on Twitter or Facebook?
<?php endif; ?>
</span>
</h1>
<div class="text-center limit-reached" style="<?php echo $setstyle; ?>">
@include('desktop.includes.spin.share', ['fbLimitReached' => $fbLimitReached, 'twitterLimitReached' => $twitterLimitReached])
@include('desktop.includes.spin.add-friend')
</div>
</div>
</div>
</div>
</div>
<!--Header Title Ends-->
</div>
</header>
<section>
<div class="spin-wheel-bg text-center">
<div class="spin-wheel">
<div class="spin-wheel-img"><img src="<?php echo themosis_assets() . '/images/spin.png'; ?>" class="img-responsive" alt="Spin And Win"></div>
<div class="spin-share-message"><span>Share with your friends to spin again<br> or come back tomorrow.</span></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="spin-wheel-content text-center">
<h4 class="h4-main">Up For Grabs</h4>
<div class="content-bottom">
<p>Fashion vouchers to the value of R50 000  |  R200 off your first Uber ride  |  Food &amp; wine vouchers to the value of R150 000  |  E-commerce vouchers to the value of R100 000  |  FREE Pet Food for a year to the value of R50 000  |  Additional product prizes valued at over R10 000</p>
</div>
<p class="terms-condition"><a href="<?php echo home_url(); ?>/terms" target="_blank" style="text-decoration:none;">Terms &amp; Conditions </a>  |  <a href="<?php echo home_url(); ?>/help" target="_blank" style="text-decoration:none;">Frequently Asked Questions</a></p>
</div>
</div>
</div>
</section>

@include('desktop.includes.spin.footer')
