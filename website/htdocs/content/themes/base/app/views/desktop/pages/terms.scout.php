@include('desktop.includes.spin.header')
<!--Header Title-->
<div class="container">
<div class="row">
<div class="header-title-bg min-padding">
<h1 class="h1-main-ttl text-center"><img src="<?php echo themosis_assets() . '/images/spinwin-logo.png'; ?>" class="img-responsive" alt="Spin And Win"><span>Spin the wheel and win! Guarenteed!</span></h1>
</div>
</div>
</div>
<!--Header Title Ends-->
</div>
</header>
<section>
<div class="container">
<div class="row">
<div class="faqs-section">
<a href="<?php echo home_url(); ?>" class="back-home">Back Home</a>
<p class="faq-text ans"><strong>1. Competition Standard Terms &amp; Conditions</strong></p>
<p class="faq-text">1.1 These terms and conditions are applicable in respect of all entrants participating in any competitions which may be run or be promoted by CompareGuru (www.compareguru.co.za) (whether on the Website, Facebook page or otherwise) along with any specific terms it publishes that may apply to each competition. These Competition Standard Terms &amp; Conditions form part of the general Terms &amp; Conditions generally for the Website and both sets of terms are expressly incorporated into any specific terms for any of our competitions.</p>
<p class="faq-text">In the event of any conflict and/or discrepancy between these Competition Standard Terms &amp; Conditions, the Terms &amp; Conditions and any specific terms for a competition, the relevant terms of a specific competition shall apply. CompareGuru or any of its associated companies reserve the right to disqualify your entry from any competition and/or withhold any prize if they have reasonable grounds to believe that you have breached any of these Competition Standard Terms &amp; Conditions, Terms &amp; Conditions or any specific rules of any competition (all three together are referred to as “Competition Terms &amp; Conditions”).</p>
<p class="faq-text">1.2 To enter a competition and to be eligible to win a prize you must:</p>
<p class="faq-text">(a) Be resident in the Republic of South Africa (unless permitted otherwise in the specific terms and conditions of a competition);</p>
<p class="faq-text">(b) Be 18 years of age or over (or of the age specified in the competition and, if minors are stated to be eligible to enter a competition, then they must have the express consent of their parent(s) or guardian);</p>
<p class="faq-text">(c) Not be an employee of CompareGuru or any of its related companies or any service provider (or employee or agent thereof) to CompareGuru or a member of the family, friend or acquaintance of any of the above; and</p>
<p class="faq-text">(d) Comply with these Terms &amp; Conditions and any rules specific to the competition. All entry instructions communicated by CompareGuru will form part of the Competition Terms &amp; Conditions.</p>
<p class="faq-text">1.3 By entering any competition and/or accepting a prize from CompareGuru you warrant that you have complied, and shall continue to comply, with these Competitions Terms &amp; Conditions. By entering the competition, you agree to take part in publicity of the competition at CompareGuru’s request and/or to allow CompareGuru to use your name, image and/or town of residence in such promotional materials as CompareGuru sees fit whether now or in the future and anywhere in the Republic of South Africa or in the world, without any further compensation.</p>
<p class="faq-text">1.4 All entries must be made in the entrant's own full names. Where applicable, CompareGuru will only issue prize cheques made payable in the winner's name as given to CompareGuru at the time of entry.</p>
<p class="faq-text">1.5 Closing Date: the closing date for a competition is the date and/or time as stated in the instructions for the respective competition and, unless otherwise stated, entries must be received by CompareGuru on the closing date by the following times (in accordance with the method as permitted in the relevant competition rules:</p>
<p class="faq-text fq-pad">(i) Midnight for entries by email and/or online; or</p>
<p class="faq-text fq-pad">(ii) 4pm for entries by post or other method; or</p>
<p class="faq-text fq-pad">(iii) Midnight for entries by telephone or SMS (unless on live or as-live programming; or</p>
<p class="faq-text fq-pad">(iv) As may be otherwise directed in respect of competitions on live or as-live programming;</p>
<p class="faq-text">1.6 Where competition entries are made by telephone or SMS, the costs of entry will be stated in the relevant specific terms of the competition.</p>
<p class="faq-text">1.7 One entry allowed per household unless expressly stated otherwise in the relevant specific terms of the competition.</p>
<p class="faq-text">1.8 There are no prize substitutions nor are cash alternatives and the prizes not transferable. CompareGuru reserves the right to select an alternative Winner and award the prize to another entrant in the event that:</p>
<p class="faq-text fq-pad">(i) It has reasonable grounds for believing that an entrant has contravened any term or condition of entry;</p>
<p class="faq-text fq-pad">(ii) A Winner has changed address to one outside of the Republic of South Africa, after the closing date;</p>
<p class="faq-text fq-pad ">(iii) It is unable to contact the Winner within 24 hours of selecting the Winner;</p>
<p class="faq-text fq-pad">(v) It does not receive confirmation of the Winner’s address as may be requested within 24 hours.</p>
<p class="faq-text">1.9 CompareGuru will have a sole and absolute discretion to decide whether or not to accept any entry and CompareGuru may rely upon its own records and information in determining whether contestants are eligible to receive a prize.</p>
<p class="faq-text">1.10 In any matters concerning any part of the competition or any part of the solution to the competition, whatsoever, CompareGuru’s decision will be final and no correspondence or discussion shall be entered into with you or with any other person acting on your behalf regarding the decision whatsoever.</p>
<p class="faq-text">1.11 A winner will generally be notified either by email, telephone or post within 14 days of the relevant closing date or such other times as may be specified by CompareGuru in its absolute discretion.</p>
<p class="faq-text">1.12 A winner has a responsibility to find out from CompareGuru what the competition prize entails and does have to assume what is included in the prize if not clear the winner.</p>
<p class="faq-text">1.13 Subject to confirmation by CompareGuru of the Winner’s address as may be requested, the Winner will generally receive the prize within six to eight weeks from the closing date or as set out in any specific terms for the respective competition.</p>
<p class="faq-text">1.14 No responsibility can be accepted for entries lost, delayed, misdirected, damaged or undelivered. Incomplete, inaudible and/or illegible entries will be disqualified. CompareGuru shall not be responsible for technical errors in telecommunications networks, internet access or otherwise preventing entry to any competition.</p>
<p class="faq-text">1.15 Where any prize involves travel, all arrangements, including but not limited to travel times and dates, shall be in accordance with the restrictions specified by CompareGuru and, unless stated otherwise in writing, shall take place within 12 months of the relevant closing date. Winner’s are advised to obtain all necessary travel insurance, obtain any necessary medical advice for their trip and confirm all necessary travel documents, including a passport, prior to any travel.</p>
<p class="faq-text">1.16 CompareGuru shall not be liable for any delay in performing or partial or total failure to perform any of its obligations to the winner(s) under these Competitions Terms if such delay or failure is caused by circumstances beyond its reasonable control including, without limitation delays, changes, disruptions, cancellations, diversions or substitutions howsoever caused including without limitation, war, terrorist action or threatened terrorist action, strikes, hostilities, civil commotions, accidents, fire, flood or any natural catastrophes arising without limitation out of or in connection with the activities of any third party and/or any form of transportation (including but not limited to flights, trains, coaches, buses, ferries, taxis or cars). For the avoidance of doubt, the winner(s) shall be solely liable for any additional costs incurred as a result.</p>
<p class="faq-text">1.17 To the extent permitted in law, CompareGuru shall not be liable to the winner(s) for any loss or damage howsoever caused (whether in contract, tort or statutory duty or otherwise) arising out of or in connection with the competition and/or prize other than death or personal injury caused by negligence of the Promoter and/or the Promoters personnel and/or deceit or fraud by Promoter and/or Promoter’s personnel while acting within the course and scope of their employment with the promoter.</p>
<p class="faq-text">1.18 Any personal information collected by CompareGuru in connection with a competition will be used in accordance with CompareGuru’s Privacy Policy. If entrants do not want CompareGuru to use their information in this way, entrants can tell CompareGuru when entering the competition or at any time by writing to CompareGuru at info@compareguru.co.za or help@playspinandwin.co.za.</p>
<p class="faq-text">1.19 Competitions Terms shall be governed by and construed in accordance with the laws of RSA and any disputes arising therefrom, shall be subject to the exclusive jurisdiction of the courts of RSA.</p>
<p class="faq-text">1.20 Please note that each user will be limited to one Facebook share and one tweet per day, but can refer an unlimited amount of friends.</p>
<p class="faq-text">1.21 Users who input emails that are recognized as spam will not be allowed to enter the competition.</p>
<p class="faq-text">1.22 Pet Heaven Spin to Win Competition Terms &amp; Conditions:</p>
<p class="faq-text fq-pad">-    Winners are only eligible for the prize if the delivery address falls within Pet Heavens delivery areas as stipulated on the Pet Heaven website.</p>
<p class="faq-text fq-pad">-    For 12 months from the date of announcement, the winner is entitled to Pet Food and Pet Treats that are available on the Pet Heaven website up to the total value of R50 000.</p>
<p class="faq-text fq-pad">-    After 12 months, the balance of the R50 000 that has not been spent is forfeited.</p>
<p class="faq-text fq-pad">-    The winner is only entitled to receiving their prize through the form of a scheduled delivery system that will be set up and managed by Pet Heaven. The prize is not claimable in any form other than stipulated above.</p>
<p class="faq-text fq-pad">-    The winner will be required to submit an accurate list of information about all of their Pets when placing their initial scheduled order. This includes a full list of products that are currently being fed to their pets.</p>
<p class="faq-text fq-pad">-    Once selected, the winner will be required to submit an order request for the products that are required to be delivered over the 12 months.</p>
<p class="faq-text fq-pad">-    The approval of all products included on the scheduled order of the winner are at the full discretion of Pet Heaven.</p>
<p class="faq-text fq-pad">-    Once the winners initial order request has been approved, no products may be added to this order unless it is to substitute a product that is already on the scheduled order. </p>
<p class="faq-text fq-pad">-    The competition is only applicable to Pet Foods and Pet Treats available on the Pet Heaven website. No other products available on the Pet Heaven website will be available to the winner as part of the competition.</p> 
<p class="faq-text fq-pad">-    The winner is only entitled to receive delivery at one address for the duration of the competition. In the event that the delivery address changes, the winner is responsible for notifying Pet Heaven.</p>
<p class="faq-text fq-pad">-    The approval of any requests made by the competition winner that may result in a variation in any way to the order initially submitted, is at the full discretion of Pet Heaven.</p>
<p class="faq-text">1.23 By entering this competition, all entrants agree to subscribe to further communication from all participating brands.</p>
<a href="<?php echo home_url(); ?>" class="back-home">Back Home</a>
</div>

</div>
</div>
</section>

@include('desktop.includes.spin.footer')
