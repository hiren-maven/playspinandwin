<?php
/* @var bool $fbLimitReached */
/* @var bool $twitterLimitReached */

$url = strtok($_SERVER['REQUEST_URI'], '?');
$requred_string = substr(strrchr($url, '/'), 1);
?>
<style type="text/css">
	.p-locked,
	.p-share{
	  display:none;
	}
</style>
<?php if($requred_string != 'sorry'){ ?>
<div class="col-md-12 col-xs-12">
	<div class="row">
		<div class="send-invites-social">
			<p class="send-invites-ttl">
			<?php 
if ((!$fbLimitReached) || (!$twitterLimitReached)){ ?>	
			Or share on social media:
			<?php } ?>
			</p>
<?php } ?>
			<ul class="social">
				<li>
					<?php if (!$fbLimitReached): ?>

				


				<a id="facebook-share" disabled href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				
					<?php endif; ?>		
				</li>
				<li>
					<?php if (!$twitterLimitReached): ?>
				<a href="https://twitter.com/intent/tweet?text=Spin+the+wheel+to+win+instant+prizes+to+the+value+of+R400+000%2C+or+be+entered+into+the+draw+to+win+BIG%21&url=<?php echo home_url(); ?>" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<?php endif; ?>
				</li>
			</ul>
			<?php 
			if($requred_string != 'sorry'){
				$social_err_msg = 'win-page-social-error';
			}else{ 
				$social_err_msg = 'sorry-page-social-error';
			} 
			?>
			<p class="p-share <?php echo $social_err_msg; ?>" >oops... something wrong in share.</p>
	        <p class="p-locked <?php echo $social_err_msg; ?>" >In order to win spin, you need to share it first!</p>
<?php if($requred_string != 'sorry'){ ?>
		</div>
	</div>
</div>
<?php } ?>
<script type="text/javascript">
	(function($){

    

			$.ajaxSetup({ cache: true });
			$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
				FB.init({
					appId: '<?php echo \Option::get('site_settings', 'facebook_app_id'); ?>',
					version: 'v2.5' // or v2.0, v2.1, v2.2, v2.3
				});
				var shareLink = $('#facebook-share');

				 var txtShare = $('.p-share'),
			         txtLocked = $('.p-locked'),
			         txtUnlocked = $('.p-unlocked');

				shareLink.removeAttr('disabled');
				shareLink.on('click', function(e) {
					e.preventDefault();
					txtShare.add(txtLocked).slideUp();
					FB.ui({
						method: 'share',
						href: '<?php echo home_url(); ?>',
					}, function(response) {
              			/** debug **/
		               console.log(typeof response);
		               console.log(response);
		               console.log(response instanceof Array);
    			     	if (response != 'undefined' && response instanceof Array) {
	    			        /** the user shared the content on their Facebook, go ahead and continue to download **/
	                         saveSocialShare('facebook');
	    			     } else {
	    			        /** the cancelled the share process, do something, for example emphasize **/
	                  		txtLocked.slideDown().delay(5000).fadeOut(400);
	    				 }

					});
				});
			});
	})(jQuery);
</script>
