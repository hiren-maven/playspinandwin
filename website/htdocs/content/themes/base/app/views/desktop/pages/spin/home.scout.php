<?php
/* @var bool $isSpinningAgain */
/* @var string|null $sharedVia */
/* @var bool|null $fbLimitReached */
/* @var bool|null $twitterLimitReached */
?>
@include('desktop.includes.spin.header')

<?php if (!$isSpinningAgain) { ?>

	<form id="entry-form" method="post" action="/api/signup">

		<!--Header Title-->
		<div class="container">
			<div class="row">
				<div class="header-title-bg min-height-bg-ttl">
					<h1 class="h1-main-ttl text-center"><img src="<?php echo themosis_assets() . '/images/spinwin-logo.png'; ?>" class="img-responsive" alt="Spin And Win"><span>Please fill in the following information to proceed...</span></h1>
					<div class="detail-form text-center">
						<h2 class="h2-main">Your Details</h2>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input class="form-control" placeholder="Full Name" name="name"/>
								<label for="name" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input class="form-control" placeholder="Age" name="age"/>
								<label for="age" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input class="form-control" placeholder="Email Address" name="email"/>
								<label for="email" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input class="form-control" placeholder="Contact No." name="contactno" maxlength='10'/>
								<label for="contactno" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label class="select-arrow">
									<select name="gender" class="form-control">
										<option class="text-center">Gender</option>
										<option class="text-center">Male</option>
										<option class="text-center">Female</option>
									</select>
									
								</label>
                                <label for="gender" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label class="select-arrow">
									<select class="form-control" name="location">
										<option>Location</option>
										<option value="western_cape">Western Cape</option>
										<option value="kzn">KwaZulu Natal</option>
										<option value="gauteng">Gauteng</option>
										<option value="other">Other</option>
									</select>
									
								</label>
                                <label for="location" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="select-arrow select-width">
									<select class="form-control" name="interests" id="interests" multiple="multiple">
										<option value="electronics">Electronics</option>
										<option value="fashion">Fashion</option>
										<option value="food">Food and wine</option>
										<option value="deals">Deals</option>
										<option value="travel">Travel</option>
										<option value="online_shopping">Online shopping</option>
										<option value="animals">Animals</option>
										<option value="family">Family</option>
									</select>
									
								</label>
                                <label for="interests" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<h2 class="h2-main">Your Friends Details</h2>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input class="form-control" placeholder="Full Name" name="friend_name"/>
								<label for="friend_name" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input class="form-control" placeholder="Email Address" name="friend_email"/>
								<label for="friend_email" generated="true" class="error" style="display: none;"></label>
							</div>
						</div>
                        <div class="col-md-12 col-xs-12">
							<div class="form-group">
                            <p>By entering this competition, all entrants agree to subscribe to further communication from all participating brands.</p>
                            <div class="spin-check-box">
                            
                            <input name="spintermcheck" id="spintermcheck" type="checkbox" value="" class="spin-term-check" /> 
								<p class="text-left spin-check">I agree to the Terms and Conditions</p>
                                
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Header Title Ends-->

	</div>
</header>

<section>
	<div class="container">
		<div class="row">
			<div class="spin-wheel-bg text-center">
				<div class="spin-wheel">
					<div class="spin-now"><button class="btn spin-btn">Spin Now</button></div>
					<canvas class="the_canvas" id="myDrawingCanvas" width="366" height="367" data-wheel-image="<?php echo themosis_assets() . '/images/spin.png'; ?>" data-spin-on="<?php echo themosis_assets() . '/images/spin_on.png'; ?>" data-spin-off="<?php echo themosis_assets() . '/images/spin_off.png'; ?>">
						<p class="noCanvasMsg" align="center">Sorry, your browser doesn't support canvas.<br />Please try another.</p>
					</canvas>
				</div>
			</div>

			<div class="spin-wheel-content text-center">
				<h4 class="h4-main">Up For Grabs</h4>
				<div class="content-bottom">
					<p>Fashion vouchers to the value of R50 000  |  R200 off your first Uber ride  |  Food &amp; wine vouchers to the value of R150 000  |  E-commerce vouchers to the value of R100 000  |  FREE Pet Food for a year to the value of R50 000  |  Additional product prizes valued at over R10 000</p>
				</div>
				<p class="terms-condition">
					<a href="<?php echo home_url().'/terms'; ?>">Terms &amp; Conditions</a>  | <a href="<?php echo home_url().'/help'; ?>">  Frequently Asked Questions</a>
				</p>
			</div>
		</div>
	</div>
</section>

</form>


<?php } else { ?>	

	<!--Header Title-->
	<div class="container">
	<div class="row">
	<div class="header-title-bg min-height-bg-ttl">
	<h1 class="h1-main-ttl text-center"><img src="<?php echo themosis_assets() . '/images/spinwin-logo.png'; ?>" class="img-responsive" alt="Spin And Win"><span>
		<h4 style="text-transform: uppercase; font-weight: bold;">Thanks for Sharing!</h4>
		<?php 
	

	if ($sharedVia == 'facebook' || $sharedVia == 'twitter') { ?>
			<p style="font-family: Lato, sans-serif; font-style: italic;">You only get one <?php echo $sharedVia; ?> share per day, visit us tomorrow to claim your next spin!</p>
			<?php if((!$fbLimitReached) || (!$twitterLimitReached)){ ?>
			<p style="font-family: Lato, sans-serif; font-style: italic;">Why not share on <?php echo $sharedVia == 'facebook' ? 'twitter' : 'facebook'; ?> next?</p>
			<?php } ?>
			<?php } else { ?>
				<p style="font-family: Lato, sans-serif; font-style: italic;">You only get one <?php echo $sharedVia; ?> share per day,<br />Why not <?php echo $sharedVia == 'facebook' ? 'twitter' : 'facebook'; ?> next?</p>
				<?php } ?>

	</span></h1>
	</div>
	</div>
	</div>
	<!--Header Title Ends-->


	</div>
</header>

				<form id="spin-again-form" method="post" action="/api/spin">
					<!--<div class="form-wrapper">
						<input type="submit" value="Spin again" class="btn spin-now-button" id="spin-again-button" />
					</div>-->
					<section>
						<div class="container">
							<div class="row">
								<div class="spin-wheel-bg text-center">
									<div class="spin-wheel">
										<div class="spin-now"><button type="submit" class="btn spin-btn">Spin again</button></div>
										<canvas class="the_canvas" id="myDrawingCanvas" width="366" height="367" data-wheel-image="<?php echo themosis_assets() . '/images/spin.png'; ?>" data-spin-on="<?php echo themosis_assets() . '/images/spin_on.png'; ?>" data-spin-off="<?php echo themosis_assets() . '/images/spin_off.png'; ?>">
											<p class="noCanvasMsg" align="center">Sorry, your browser doesn't support canvas.<br />Please try another.</p>
										</canvas>
									</div>
								</div>

								<div class="spin-wheel-content text-center">
									<h4 class="h4-main">Up For Grabs</h4>
									<div class="content-bottom">
										<p>Fashion vouchers to the value of R50 000  |  R200 off your first Uber ride  |  Food &amp; wine vouchers to the value of R150 000  |  E-commerce vouchers to the value of R100 000  |  FREE Pet Food for a year to the value of R50 000  |  Additional product prizes valued at over R10 000</p>
									</div>
									<a href="<?php echo home_url().'/terms'; ?>">Terms &amp; Conditions</a>  | <a href="<?php echo home_url().'/help'; ?>">  Frequently Asked Questions</a>
								</div>
							</div>
						</div>
					</section>
				</form>
		


			<?php } ?>

			@include('desktop.includes.spin.footer')