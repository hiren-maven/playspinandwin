<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Spin & Win - Data Dump</title>
</head>
<body>

	<form method="post" action="/api/downloadAuth">

		<label for="username">Username:</label>
		<input id="username" type="text" name="username" />

		<label for="password">Password:</label>
		<input id="password" type="password" name="password" />

		<input type="submit" value="Download" />

	</form>

</body>
