@include('mailer.competition.header')
    <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
    <tr>
    <td align="center" style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/tagline.png"></td>
    </tr>
    <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>

<tr>
	<td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">A chance to get your hands on <br/>One of the following :</td>
</tr>
<tr>
	<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
</tr>
<tr>
   <td align="center" style="color:#333;font-size:12px;font-weight:400;text-align:center;font-family:'Lato', sans-serif;">
Roald Dahl Box Set (R1 200)<br>
Beatrix Potter Set (R1 500)<br>
Winnie the Pooh Set (R3 000)</td>
</tr>
<tr>
	<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
</tr>
<tr>
	<td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/competitions/odo-kids-draw.png"></td>
</tr>
<tr>
	<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
</tr>
<tr>
	<td align="center" style="text-align:center;font-size:12px;line-height:22px">Visit www.onedayonly.co.za for more great deals<br>Winners will be notified via email within a month after the competition closing date 31 october 2016<br/> Delivery will take place within two weeks of receiving winner's details.<br>T’s &amp; C’s Apply<br><i>Visit <a href="http://www.playspinandwin.co.za/help" target="_blank" style="text-decoration:none;cursor:pointer;">www.playspinandwin.co.za/help</a> for FAQ or email <a style="text-decoration:none;cursor:pointer;" href="mailto:help@playspinandwin.co.za" target="_blank">help@playspinandwin.co.za</a></i></td>
</tr>
<tr>
	<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="60"></td>
</tr>
<tr>
	<td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Share on social media and spin again</td>
</tr>
@include('mailer.competition.footer')
