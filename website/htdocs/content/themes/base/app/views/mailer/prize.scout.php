<!DOCTYPE html>
<html>
<head>
	<title>Prize</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body style="font-family: 'Lato', sans-serif;">

<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td align="center">

			<br />

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td background="<?php echo themosis_assets() . '/images/mailer-background.jpg'; ?>" bgcolor="#ffffff" valign="top" align="center" width="600">
						<!--[if gte mso 9]>
						<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;">
							<v:fill type="tile" src="<?php echo themosis_assets() . '/images/mailer-background.jpg'; ?>" color="#ffffff" />
							<v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
						<![endif]-->

						<table width="80%" border="0" cellpadding="0" cellspacing="0">
							<tr style="text-align: center;">
								<td style="width: 100%;">
									<img src="<?php echo themosis_assets() . '/images/mailer/logos.jpg'; ?>" class="img-responsive participating-logo" alt="" />
								</td>
							</tr>
						</table>

						<br />

						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td></td>
								<td rowspan="2">
									<div class="row">
										<div class="container">
											<table width="100%" height="100" border="0" cellpadding="0" cellspacing="0"></table>
											<img src="<?php echo themosis_assets() . '/images/mailer-prizes.png'; ?>" class="img-responsive participating-logo" alt=""/>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center" style="vertical-align: top;">
									<div style="font-style: italic;" class="row">
										<div class="container">
											<a href="<?php echo home_url(); ?>" style="text-decoration: none;"><img src="<?php echo themosis_assets() . '/images/mailer/spinandplay.jpg'; ?>" class="campaign-logo" alt=""/></a>
										</div>
									</div>
								</td>
							</tr>
						</table>

						<table width="100%" border="0" cellpadding="0" cellspacing="0" style="text-align: center;">
							<tr>
								<td>
									<!-- START || DYNAMIC PART HERE FOR BE DEV TO IMPLEMENT -->
									<h3 style="font-size: 22px; text-transform: uppercase; margin: 10px 0;">UP FOR GRABS</h3>
									<p style="margin: 5px 0 20px;">
										5 Nights all-inclusive trip to Mauritius for 2 people<br>
										Instant vouchers to the value of R250 000<br>
										Electronic prizes to the value of R10 000<br>
										60 Entertainer Apps<br>
										Golf tour to the San Lameer Hotel &amp; Spa for 4 people for 4 days
									</p>
									<!-- END || DYNAMIC PART HERE FOR BE DEV TO IMPLEMENT -->
								</td>
							</tr>
						</table>

						<!--[if gte mso 9]>
						</v:textbox>
						</v:rect>
						<![endif]-->
					</td>
				</tr>
			</table>

			<br />

			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="center">
						<table width="300" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right">
									<span style="font-style: italic;"><em>Share now</em></span>
								</td>
								<td align="center">
									
									<a style="text-decoration: none" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo home_url(); ?>&t=Click+Compare+-+Spin+%26+Win" target="_blank" title="Share on Facebook">
										<img src="<?php echo themosis_assets() . '/images/fb-button.png'; ?>" style="width: 50px;" />
									</a>
									<a style="text-decoration: none" href="https://twitter.com/intent/tweet?text=Spin+the+wheel+to+win+instant+prizes+to+the+value+of+R150+000%2C+or+be+entered+into+the+draw+to+win+BIG%21&url=<?php echo home_url(); ?>" target="_blank">
										<img src="<?php echo themosis_assets() . '/images/twitter-button.png'; ?>" style="width: 50px;" />
									</a>												
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>			

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">

						<table width="300" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<p style="font-style: italic; margin: 45px 0 15px;"><em>Brought to you by:</em></p>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr style="text-align: center;">
											<td style="width: 100%;">
												<img src="<?php echo themosis_assets() . '/images/mailer/logos.jpg'; ?>" alt="" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>

			<br />

		</td>
	</tr>
</table>

</body>
</html>