<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Spin n Win</title>
<link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">

<script type="text/javascript">
  (function($){
      $.ajaxSetup({ cache: true });
      $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
        FB.init({
          appId: '<?php echo \Option::get('site_settings', 'facebook_app_id'); ?>',
          version: 'v2.5' // or v2.0, v2.1, v2.2, v2.3
        });
        var shareLink = $('#facebook-share');
        shareLink.removeAttr('disabled');
        shareLink.on('click', function(e) {
          e.preventDefault();
          FB.ui({
            method: 'share',
            href: '<?php echo home_url(); ?>'
          }, function(response) {
              if (response != undefined) {
                  saveSocialShare('facebook');
              }
              else {
                console.log('### Never actually shared :(');
              }
          });
        });
    });
  })(jQuery);
</script>

</head>

<body style="margin:0;padding:0;font-family: 'Lato', sans-serif;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(<?php echo themosis_assets(); ?>/images/mailer/dynamic/cloud-bg.jpg);background-repeat:no-repeat;">
  <tr>
    <td>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(<?php echo themosis_assets(); ?>/images/mailer/dynamic/logos-bg.png);background-repeat:repeat;background-position:center center">
  <tr>
    <td>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><a href="http://compareguru.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/compareguru-logo.png"></a></td>
    <td align="center"><a href="http://www.cybercellar.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/cybercellar-logo.png"></a></td>
    <td align="center"><a href="http://www.pricecheck.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/pricecheck-logo.png"></a></td>
    <td align="center"><a href="http://www.ucook.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/ucook-logo.png"></a></td>
    <td align="center"><a href="http://www.spree.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/spree-logo.png"></a></td>
    <td align="center"><a href="http://www.uber.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/uber-logo.png"></a></td>
    <td align="center"><a href="http://www.onedayonly.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/onedayonly-logo.png"></a></td>
    <td align="center"><a href="http://www.petheaven.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/petheaven-logo.png"></a></td>
    <td align="center"><a href="http://www.sweepsouth.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/sweepsouth-logo.png"></a></td>
  </tr>
</table>
    </td>
    </tr>
    </table>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png"></td>
  </tr>  
  <tr>
    <td align="center" style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/logo.png"></td>
    </tr>
    <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
    <tr>
    <td align="center" style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/tagline.png"></td>
    </tr>
    <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
