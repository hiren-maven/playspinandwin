<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Your friend has entered!</title>
<link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
</head>

<body style="margin:0;padding:0;font-family: 'Lato', sans-serif;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(<?php echo themosis_assets(); ?>/images/mailer/dynamic/cloud-bg.jpg);background-repeat:no-repeat;background-position:center -150%">
  <tr>
    <td>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(<?php echo themosis_assets(); ?>/images/mailer/dynamic/logos-bg.png);background-repeat:repeat;background-position:center center">
  <tr>
    <td>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><a href="http://compareguru.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/compareguru-logo.png"></a></td>
    <td align="center"><a href="http://www.cybercellar.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/cybercellar-logo.png"></a></td>
    <td align="center"><a href="http://www.pricecheck.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/pricecheck-logo.png"></a></td>
    <td align="center"><a href="http://www.ucook.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/ucook-logo.png"></a></td>
    <td align="center"><a href="http://www.spree.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/spree-logo.png"></a></td>
    <td align="center"><a href="http://www.uber.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/uber-logo.png"></a></td>
    <td align="center"><a href="http://www.onedayonly.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/onedayonly-logo.png"></a></td>
    <td align="center"><a href="http://www.petheaven.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/petheaven-logo.png"></a></td>
    <td align="center"><a href="http://www.sweepsouth.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/sweepsouth-logo.png"></a></td>
  </tr>
</table>
    </td>
    </tr>
    </table>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png"></td>
  </tr>  
  <tr>
    <td align="center" style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/logo.png"></td>
    </tr>
    <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
    <tr>
   <td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Your friend, <?php echo $friend->name; ?> , just entered and won!</td>
    </tr>
   <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
  <tr>
   <td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;"><span style="font-size:50px">
    <a href="http://www.playspinandwin.co.za" target="_blank">
    <img src="<?php echo themosis_assets() . '/images/playnow-btn.jpg'; ?>">
  </a>
   </span></td>
    </tr>
    <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
   <tr>
   <td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Share on social media and spin again</td>
   </tr>
   <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
   <tr>
   <td align="center"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo home_url(); ?>&t=Click+Compare+-+Spin+%26+Win" target="_blank" title="Share on Facebook" style="width:75px;display:inline-block;text-align:center"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/facebook-icon.png" width="63" height="63"></a><a style="width:75px;display:inline-block;text-align:center" href="https://twitter.com/intent/tweet?text=Spin+the+wheel+to+win+instant+prizes+to+the+value+of+R400+000%2C+or+be+entered+into+the+draw+to+win+BIG%21&url=<?php echo home_url(); ?>" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/twitter-icon.png" width="63" height="63"></a></td>
   </tr>
   <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="40"></td>
  </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
    <td align="center" style="color:#333;font-size:16px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Up  for  grabs:</td>
    </tr>
    <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
    <tr>
    <td align="center" style="color:#333;font-size:12px;line-height:22px;font-weight:400;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Fashion vouchers to the value of R50 000  |  R200 off your first Uber ride  |  Food &amp; wine vouchers to the value of R150 000  |  E-commerce vouchers to the value of R100 000  |  FREE Pet Food for a year to the value of R50 000  |  Additional product prizes valued at over R10 000</td>
    </tr>
     <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="20"></td>
  </tr>
    <tr>
    <td>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><a href="http://compareguru.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/compareguru-logo.png"></a></td>
      <td><a href="http://www.cybercellar.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/cybercellar-logo.png"></a></td>
      <td><a href="http://www.pricecheck.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/pricecheck-logo.png"></a></td>
      <td><a href="http://www.ucook.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/ucook-logo.png"></a></td>
      <td><a href="http://www.spree.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/spree-logo.png"></a></td>
      <td><a href="http://www.uber.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/uber-logo.png"></a></td>
      <td><a href="http://www.onedayonly.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/onedayonly-logo.png"></a></td>
      <td><a href="http://www.petheaven.co.za/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/petheaven-logo.png"></a></td>
      <td><a href="http://www.sweepsouth.com/" target="_blank"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/sweepsouth-logo.png"></a></td>
  </tr>
</table>
    </td>
  </tr>
    </table>
    </td>
    </tr>
</table>

    </td>
  </tr>
</table>

</body>
</html>
