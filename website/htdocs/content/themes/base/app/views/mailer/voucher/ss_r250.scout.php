<?php
/* @var $voucher \Model\VoucherModel */
?>
@include('mailer.voucher.header')
<tr>
   <td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">A sweepsouth.Com <?php echo $voucher->value; ?> VOUCHER</td>
    </tr>
    <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
  </tr>
  <tr>
   <td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Code:<br><span style="font-size:50px"><?php echo $voucher->code; ?></span></td>
    </tr>
  <tr>
   <td align="center" style="color:#333;font-size:12px;font-weight:400;text-align:center;font-family:'Lato', sans-serif;">To redeem, insert the coupon code while booking on <a href="http://www.sweepsouth.com/" target="_blank">http://www.sweepsouth.com</a> or via the SweetSouth app. Valid for new customers only. Valid until 31 January 2017.</td>
   </tr>
   <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="30"></td>
  </tr>
   <tr>
  <td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/sweepsouth.png"></td>
  </tr>
  <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="30"></td>
  </tr>
  <tr>
  <td align="center" style="text-align:center;font-size:12px;line-height:22px">Visit www.sweepsouth.com for more great deals<br>T’s &amp; C’s Apply<br><i>Visit <a href="http://www.playspinandwin.co.za/help" style="text-decoration:none;color:#333333;">www.playspinandwin.co.za/help</a> for FAQ or email <a href="mailto:help@playspinandwin.co.za" target="_blank">help@playspinandwin.co.za</a></i></td>
  </tr>
  <tr>
  <td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="60"></td>
  </tr>
@include('mailer.voucher.footer')
