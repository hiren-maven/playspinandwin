@include('mailer.voucher.header')
 <tr>
<td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Get r200 off your first uber ride!</td>
</tr>
<tr>
<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="10"></td>
</tr>
<tr>
<td align="center" style="color:#333;font-size:20px;font-weight:900;text-transform:uppercase;text-align:center;font-family:'Lato', sans-serif;">Code:<br><span style="font-size:50px">Spinwin</span></td>
</tr>
<tr>
<td align="center" style="color:#333;font-size:12px;font-weight:400;text-align:center;font-family:'Lato', sans-serif;">Please note that you need to be a first time user to use this voucher.<br> If you have used a voucher
previously please gift this one to one of your friends.</td>
</tr>
<tr>
<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="30"></td>
</tr>
<tr>
<td style="text-align:center;"><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/uber_r200.png"></td>
</tr>
<tr>
<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="30"></td>
</tr>
<tr>
<td align="center" style="text-align:center;font-size:12px;line-height:22px">Visit www.uber.com for more great deals<br>T’s &amp; C’s Apply<br><i>Visit <a href="http://www.playspinandwin.co.za/help" style="text-decoration:none;color:#333333;">www.playspinandwin.co.za/help</a> for FAQ or email <a href="mailto:help@playspinandwin.co.za" target="_blank">help@playspinandwin.co.za</a></i></td>
</tr>
<tr>
<td><img src="<?php echo themosis_assets(); ?>/images/mailer/dynamic/blank.png" height="60"></td>
</tr>

@include('mailer.voucher.footer')