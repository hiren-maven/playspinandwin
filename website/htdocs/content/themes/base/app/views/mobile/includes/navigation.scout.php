@include('mobile.includes.header')

<div class="container">
    <div class="nav_link">
        <a href="javascript:history.back()">back</a>
    </div>
    <div class="nav_link">
        <a href="<?php echo home_url(); ?>">Home</a>
    </div>
    	<div id="menu" class="feature_menu">
          	<?php
            	wp_nav_menu(array(
              		'theme_location' => 'header-nav',
              		'container'      => false,
              		'menu_class'  => 'feature_nav',
            	));
          	?>
    	</div>
</div>

@include('mobile.includes.footer')
