<?php

return array(

    /**
     * Mapping for all classes without a namespace.
     * The key is the class name and the value is the
     * absolute path to your class file.
     *
     * Watch your commas...
     */

    /***
     * DEVELOPER NOTE:
     * We no longer need to manually load the models & controllers as they are autoloaded by composer
     ***/


    // Controllers
    //'BaseController'        => themosis_path('app').'controllers'.DS.'BaseController.php',



    // Models
    //'PostModel'             => themosis_path('app').'models'.DS.'PostModel.php',
);