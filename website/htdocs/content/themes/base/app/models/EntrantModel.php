<?php
namespace Model;

/**
 * EntrantModel
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $age
 * @property string $gender
 * @property string $interests
 * @property string $location
 * @property integer $optin
 * @property integer $can_spin
 * @property integer $available_spins
 * @property string $source
 * @property string $last_fb_share
 * @property string $last_twitter_share
 * @property string $created_date
 * @property string $updated_date
 *
 * Relations:
 * @property \Illuminate\Database\Eloquent\Collection $entries
 * @property \Illuminate\Database\Eloquent\Collection $vouchers
 */
class EntrantModel extends BaseModel
{
	/**
	 * string The wordpress post_type (optional)
	 */
	protected $wp_post_type = null;
	protected $table = 'wp_competition_entrant_details';
	protected $primaryKey = 'id';
	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'updated_date';

	public $fillable = ['name', 'email', 'age', 'gender', 'interests', 'location','contactno', 'source', 'optin'];
	public $defaultAttributes = ['optin' => '0', 'can_spin' => '0', 'available_spins' => '1'];

	/**
	 * @param array $data
	 * @return bool
	 */
	public function signup($data)
	{
		$rules = [
			'required' => ['name', 'email', 'age', 'gender', 'interests', 'location'],
			'email' => ['email'],
			'notExists' => ['email'],
			'integer' => ['age'],
			'array' => ['interests'],
			'in' => [
				(object)[
					'attribute' => 'gender',
					'params' => ['male', 'female']
				],
				(object)[
					'attribute' => 'location',
					'params' => ['western_cape', 'kzn', 'gauteng', 'other'],
				],
			],
			'boolean' => ['optin'],
		];

		if (isset($data['email'])) {
			$data['email'] = strtolower($data['email']);
			$data['email'] = $this->removePlusFromEmail($data['email']);
		}
		if (isset($data['gender'])) {
			$data['gender'] = strtolower($data['gender']);
		}

		if (parent::validate($data, $rules)) {
			// convert interests from array to string
			$data['interests'] = implode(',', $data['interests']);
			$this->fill($data)->save();
			$this->wasInvited();

			/** send invite to friend **/
	        if($data['friendname'] != ''){
	        	$friend = new FriendModel;
                $friend->setAttributes([
                    'entrant_id' => $this->id,
                    'email' => strtolower($data['friendemail']),
                    'name' => $data['friendname'],
                ]);

                if ($friend->save()) {
                    $this->sendFriendInvite($friend);
                }            
            }
            /** send invite to friend **/
	     
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param array $data
	 * @param bool $enableSpin
	 * @return bool
	 */
	public function addFriend($data, $enableSpin = true)
	{
		if (!isset($data['friend_email'])) {
			return false;
		}

		// secure against plus signs in the email
		$data['friend_email'] = $this->removePlusFromEmail($data['friend_email']);

		// check for duplication attempts
		$exists = FriendModel::findByEntrantAndEmail($this, $data['friend_email']);
		if (!empty($exists)) {
			$this->errors['friend_email'] = 'You\'ve already added this friend';
			return false;
		}

		//uncomment this line after development is done
		$isReal = BaseModel::emailExists($data['friend_email'], 'help@playspinandwin.co.za');
		if ($isReal == 'invalid') {
			$this->errors['friend_email'] = 'This email address does not exist';
			return false;
		}

		$friend = new FriendModel;
		$friend->setAttributes([
			'entrant_id' => $this->id,
			'email' => strtolower($data['friend_email']),
			'name' => $data['friend_name'],
		]);

		if ($friend->save()) {
			if ($enableSpin) {
				$this->enableSpin();
			}
			$this->sendFriendInvite($friend);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param string $email
	 * @return string
	 */
	public static function removePlusFromEmail($email)
	{
		$plus = strpos($email, '+');
		$at = strpos($email, '@');
		if ($plus !== false && $at !== false && $plus < $at) {
			return substr($email, 0, $plus) . substr($email, $at);
		} else {
			return $email;
		}
	}

	/**
	 * Share to a platform, will check if platform is valid, and whether the share will earn a spin.
	 *
	 * @param string $platform
	 * @return bool
	 */
	public function share($platform)
	{
		// check if platform is valid
		if (!SocialShareModel::isPlatform($platform)) {
			return false;
		}

		// check if entrant has shared in the last 24 hours
		$hasShared = $this->hasShared($platform);

		// create and save social share record
		$socialShare = new SocialShareModel;
		$socialShare->setAttributes([
			'entrant_id' => $this->id,
			'platform' => $platform,
			'earned_spin' => $hasShared ? 0 : 1,
		]);
		$socialShare->save();

		// if entrant hasn't shared in the last 24 hours, allow them to spin again
		if (!$hasShared) {
			// update the last shared attribute
			$attr = $platform == 'facebook' ? 'last_fb_share' : 'last_twitter_share';
			$this->setAttribute($attr, date('Y-m-d H:i:s'));
			$this->save();

			//$this->enableSpin();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param string $platform
	 * @return bool
	 */
	public function hasShared($platform)
	{
		// get the last share value
		if ($platform === 'facebook') {
			$lastShare = $this->last_fb_share;
		} elseif ($platform === 'twitter') {
			$lastShare = $this->last_twitter_share;
		} else {
			return false;
		}

		if ($lastShare !== null) {
			$now = new \DateTime(date('Y-m-d H:i:s'));
			$lastShare = new \DateTime($lastShare);
			$diff = $lastShare->diff($now);
			if ($diff->days == 0) {
				$attr = $platform == 'facebook' ? 'last_fb_share' : 'last_twitter_share';
				$this->errors[$attr] = 'Sorry, you have reached your ' . ucfirst($platform) . ' shares for the day.';
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @param CompetitionModel $competition
	 */
	public function enterCompetition($competition)
	{
		$entry = new CompetitionEntrantModel;
		$entry->setAttributes([
			'competition_id' => $competition->id,
			'entrant_id' => $this->id,
		]);
		$entry->save();

		// update can_spin
		//$this->disableSpin();
		$this->sendCompetitionEmail($competition);
	}

	/**
	 * @param VoucherModel $voucher
	 */
	public function assignVoucher($voucher)
	{	
		if($voucher->type != 'Uber'){
			$voucher->exists = true;
			$voucher->setAttribute('winner_id', $this->id);
			$voucher->setAttribute('won_at', date('Y-m-d H:i:s'));
			$voucher->save();	
		}
		// update can_spin
		//$this->disableSpin();
		$this->sendVoucherEmail($voucher);
	}

	/**
	 * Enable spinning for this entrant.
	 */
	public function enableSpin()
	{
		if ($this->can_spin != 1) {
			$this->exists = true;
			$this->setAttribute('can_spin', 1);
			$this->save();
		}
	}

	/**
	 * Disable spinning for this entrant.
	 */
	public function disableSpin()
	{
		if ($this->can_spin != 0) {
			$this->exists = true;
			$this->setAttribute('can_spin', 0);
			$this->save();
		}
	}

	/**
	 * @return bool
	 */
	public function canSpin()
	{
		return $this->hasSpins();
	}

	/**
	 * @return bool
	 */
	public function hasSpins()
	{
		return ($this->available_spins > 0);
	}

	/**
	 * @return integer
	 */
	public function decrementSpins()
	{
		$this->exists = true;
		$available_spins = $this->available_spins - 1;
		$this->setAttribute('available_spins', $available_spins);
		$this->save();
		return $this->availabe_spins;
	}

	/**
	 * @return integer
	 */
	public function incrementSpins()
	{
		$this->exists = true;
		$available_spins = $this->available_spins + 1;
		$this->setAttribute('available_spins', $available_spins);
		$this->save();
		return $this->availabe_spins;
	}

	/**
	 * Check if this entrant was invited, and if so mark the invites.
	 */
	protected function wasInvited()
	{
		$invited = FriendModel::findByEmail($this->email);
		foreach ($invited as $model) {

			//Find the inviter and increment his spins, yay!
			$inviter = EntrantModel::where('id', '=', $model->entrant_id)->first();
			$inviter->incrementSpins();
			$this->sendFriendSpun($inviter, $model);

			/* @var $model FriendModel */
			$model->hasEntered();
		}
	}

	public function maxPendingFriends()
	{
		$count = FriendModel::countPendingByEntrant($this);
		if ($count > 100) return true;
		return false;
	}

	/**
	 * @param VoucherModel $voucher
	 * @return bool
	 */
	public function sendVoucherEmail($voucher)
	{
		$mailerTemplates = [
			'OneDayOnly' => [
				'100' => 'odo_r100',
			],
			'SweepSouth' => [
				'R50' => 'ss_r50',
				'R100' => 'ss_r100',
				'R250' => 'ss_r250',
				'R1000' => 'ss_r1000'
			],
			'CyberCellar' => [
				'R50 off R250' => 'cc_r50_min',
				'R 50' => 'cc_r50',
				'R 75' => 'cc_r75',
				'R 100' => 'cc_r100',
				'R 150' => 'cc_r150',
				'R 200' => 'cc_r200',
			],
			'Ucook' => [
				'R100' => 'ucook_100',
				'R300' => 'ucook_300',
				'R500' => 'ucook_500',
				'R1000' => 'ucook_1000'
			],
			'Uber'=> [
				'200'=>'uber_r200',
			],
		];

		// check for type and subtype
		if (
			!array_key_exists($voucher->type, $mailerTemplates) ||
			!array_key_exists($voucher->subtype, $mailerTemplates[$voucher->type])
		) {
			return false;
		} else {
			$template = $mailerTemplates[$voucher->type][$voucher->subtype];
		}

		$chk = $this->sendEntrantEmail('Spin & Win: Lucky You', 'mailer.voucher.' . $template, ['voucher' => $voucher]);

		return $chk;
	}

	/**
	 * @param CompetitionModel $competition
	 * @return bool
	 */
	public function sendCompetitionEmail($competition)
	{
		return $this->sendEntrantEmail('Spin & Win: Lucky You', 'mailer.competition.' . strtolower($competition->slug), ['competition' => $competition]);
	}

	/**
	 * @param FriendModel $friend
	 * @return bool
	 */
	public function sendFriendInvite($friend)
	{
		$params = [
			'entrant' => $this,
			'friend' => $friend,
		];
		
		$sources = [
			'Spinandwin_pc' => 'PriceCheck',
			'Spinandwin_pcR' => 'PriceCheck',
			'Spinandwin_cg' => 'CompareGuru',
			'Spinandwin_cgR' => 'CompareGuru',
			'Spinandwin_s' => 'Spree',
			'Spinandwin_sR' => 'Spree',
			'Spinandwin_odo' => 'OneDayOnly',
			'Spinandwin_odoR' => 'OneDayOnly',/*
			'Spinandwin_odo' => 'OneDayOnlyADULTDraw',
			'Spinandwin_odoR' => 'OneDayOnlyADULTDraw',
			'Spinandwin_odo' => 'OneDayOnlyCHILDDraw',
			'Spinandwin_odoR' => 'OneDayOnlyCHILDDraw',*/
			'Spinandwin_cc' => 'CyberCellar',
			'Spinandwin_ccR' => 'CyberCellar',
			'Spinandwin_u' => 'UCook',
			'Spinandwin_uR' => 'UCook',
			'Spinandwin_ph' => 'PetHeaven',
			'Spinandwin_phR'=>'PetHeaven',
			'Spinandwin_ss' => 'SweepSouth',
			'Spinandwin_ssR'=>'SweepSouth',
		];

		if (array_key_exists($this->source, $sources)) {
			$params['trackingQuery'] = '?utm_source=' . $sources[$this->source] . '&utm_medium=Referral&utm_campaign=' . $this->source . (substr($this->source, -1) != 'R' ? 'R' : '');
		} else {
			$params['trackingQuery'] = '';
		}

		// send the email, and return it's success status
		return $this->sendEmail($friend->name . '<' . $friend->email . '>', 'A friend has invited you to play Spin & Win!', 'mailer.friend-invite', $params);
	}

	/**
	 * @param EntryModel $inviter
	 * @param FriendModel $friend
	 * @return bool
	 */
	public function sendFriendSpun($inviter, $friend)
	{
		$params = [
			'inviter' => $inviter,
			'friend' => $friend,
		];

		// send the email, and return it's success status
		return $this->sendEmail($inviter->name . '<' . $inviter->email . '>', 'Your friend has just entered Spin & Win, and earned you another spin!', 'mailer.friend-spin', $params);
	}

	/**
	 * @param string $subject
	 * @param string $template
	 * @param array $params
	 * @param array $headers
	 * @return bool
	 */
	public function sendEntrantEmail($subject, $template, $params = [], $headers = [])
	{
		$params['entrant'] = $this;

		//echo '<pre>'; print_r($params); die; 

		// send the email, and return it's success status
		return $this->sendEmail($this->name . ' <' . $this->email . '>', $subject, $template, $params, $headers);
	}

	/**
	 * Find and populate an instance of EntrantModel by email.
	 *
	 * @param string $email
	 * @return null|static
	 */
	public static function findByEmail($email)
	{
		$query = static::query()->where('email', '=', $email)->first();

		return $query;
	}

	
	public function entries()
	{
		return $this->hasMany('Model\CompetitionEntrantModel', 'entrant_id', 'id');
	}

	public function vouchers()
	{
		return $this->hasMany('Model\VoucherModel', 'winner_id', 'id');
	}
}
