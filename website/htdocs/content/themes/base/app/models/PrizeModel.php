<?php
namespace Model;

class PrizeModel
{
	/**
	 * @var EntrantModel $entrant
	 */
	protected $entrant;

	/**
	 * An index for prize numbers on the spinning wheel.
	 *
	 * @param string|null $type
	 * @return array
	 */
	public static function prizeIndexes($type = null)
	{
		// change this array to match the prizes shown on the spin wheel
		$prizeIndexes = [
			'comp' => [
				'Spree' => 3,
				'PetHeaven'=>4,
				'OneDayOnlyCHILDDRAW' => 6,
				'OneDayOnlyADULTDRAW' => 6,
			],
			'voucher' => [
				'Ucook' => 0,
				'Uber' => 1,
				'SweepSouth'=>2,
				'OneDayOnly' => 5,
				'CyberCellar' =>7,
			],
		];
		return $type === null ? $prizeIndexes : $prizeIndexes[$type];
	}

	/**
	 * @param EntrantModel $entrant
	 */
	public function __construct($entrant)
	{
		$this->entrant = $entrant;
	}

	/**
	 * @param EntrantModel $entrant
	 * @return array
	 */
	public static function givePrize($entrant)
	{
		return (new static($entrant))->determinePrize();
	}

	/**
	 * Choose a prize for the entrant.
	 *
	 * @return array
	 */
	public function determinePrize()
	{
		$prize = \Option::get('site_settings', 'hard_prize');
		
		if ($prize) {
			if (array_key_exists($prize, static::prizeIndexes('comp'))) {
				// competition
				return $this->giveSetCompetition($prize, static::prizeIndexes('comp')[$prize]);
			} elseif (array_key_exists($prize, static::prizeIndexes('voucher'))) {
				// voucher
				return $this->giveSetVoucherType($prize, static::prizeIndexes('voucher')[$prize]);
			}
		}

		// using randomization and modulus to distribute competitions:vouchers based on a 1:X ratio
		// simply get a random number and the modulus of it and X + 1 will be equal to 0 the right amount of times
		$x = 1;

		$random_number = (mt_rand(1, 999999) % ($x + 1));

		if ($random_number == 0) {
			return $this->chooseCompetition();
		} else {
			// if entrant has any vouchers, we shall enter them into a competition
			$vouchers = $this->entrant->vouchers->all();

			if (!empty($vouchers)) {
				return $this->chooseCompetition();
			} else {
				return $this->chooseVoucher();
			}
		}
	}

	/**
	 * @param string $comp
	 * @param integer $wheelIndex
	 * @return array
	 */
	public function giveSetCompetition($comp, $wheelIndex)
	{
		$chosenCompetition = CompetitionModel::findBySlug($comp);

		// enter the competition
		$this->entrant->enterCompetition($chosenCompetition);

		// prepare a url slug
		$urlSlug = 'comp/' . $chosenCompetition->slug;

		return $this->createPrizeResponse($urlSlug, $wheelIndex);
	}

	/**
	 * @param string $type
	 * @param integer $wheelIndex
	 * @return array
	 */
	public function giveSetVoucherType($type, $wheelIndex)
	{
		// find a subtype still available to assign
		$subtypes = VoucherModel::getSubTypes($type, true);

		// prepare list of options
		$subtypeOptions = [];
		foreach ($subtypes as $subtype) {
			$subtypeOptions[] = $subtype;
		}

		// chose an option if any are available, else set to null
		if (!empty($subtypeOptions)) {
			$chosenSubtype = $subtypeOptions[mt_rand(0, count($subtypeOptions) - 1)];
		} else {
			$chosenSubtype = null;
		}

		// load up the first of that type & subtype
		$voucher = VoucherModel::firstAvailableOfType($type, $chosenSubtype);

		// small catch for cases where there suddenly aren't any available
		if ($voucher === null) {
			return $this->chooseCompetition();
		}

		// assign one of those voucher codes to the entrant (breaks until mailers are fixed)
		$this->entrant->assignVoucher($voucher);

		// prepare a url slug
		$urlSlug = 'voucher/' . $voucher->type . '/' . $voucher->subtype;


		return $this->createPrizeResponse($urlSlug, $wheelIndex);
	}

	/**
	 * Creates a standardized prize response object to be sent back to the wheel spinner.
	 *
	 * @param string $slug
	 * @param integer $wheelIndex
	 * @return array
	 */
	public function createPrizeResponse($slug, $wheelIndex)
	{
		return [
			'slug' => $slug,
			'wheelIndex' => $wheelIndex,
		];
	}

	/**
	 * Choose a competition to enter at random.
	 *
	 * @return array
	 */
	protected function chooseCompetition()
	{
		// get competition options
		$competitionOptions = $this->getCompetitionOptions();


		// get a random competition option
		$chosenKey = mt_rand(0, count($competitionOptions) - 1);

		//$chosenKey = 2;

		$chosenCompetition = $competitionOptions[$chosenKey];

		// enter the competition
		$this->entrant->enterCompetition($chosenCompetition->model);

		// prepare a url slug
		$urlSlug = 'comp/' . $chosenCompetition->model->slug;

		// return the prize response
		return $this->createPrizeResponse($urlSlug, $chosenCompetition->wheelIndex);
	}

	/**
	 * Calculate available vouchers, whilst ensuring they don't run out too fast.
	 * Then assign one type at random.
	 * Falls back to {{static::chooseCompetition}} if no vouchers are available.
	 *
	 * @return array
	 */
	protected function chooseVoucher()
	{
		// get days
		$days = $this->getDaysTillEnd();
		// get voucher counts
		$voucherCounts = $this->getVoucherCounts();

		// setup a percentage offset so that distribution is slightly accelerated
		$offsetPercentage = 10;
		$voucherTypes = [];
		foreach ($voucherCounts->avail as $type => $data) {
			// check if there is a wheel index for this voucher type
			if (!array_key_exists($type, static::prizeIndexes('voucher'))) {
				continue;
			}

			$count = $data->{'COUNT(*)'};

			// total available today
			$avail = ceil($count / $days);

			// add an offset based on total available
			if ($days > 1) {
				$avail += ceil($avail / 100 * $offsetPercentage);
			}

			// determine remaining available today
			if (array_key_exists($type, $voucherCounts->won)) {
				// some of this type of voucher have been won today
				// check if we still have any available, and how many
				$wonTodayCount = $voucherCounts->won[$type]->{'COUNT(*)'};
				$avail -= $wonTodayCount;
			}

			// if there are still any available
			if ($avail > 0) {
				$voucherTypes[] = $type;
			}
		}

		// if we are out of vouchers, award the entrant with a competition entry rather
		if (empty($voucherTypes)) {
			return $this->chooseCompetition();
		}

		// get a random voucher type from the pool of remaining options
		$voucherType = $voucherTypes[mt_rand(0, count($voucherTypes) - 1)];

		/** code for uber voucher **/
		if($voucherType == 'Uber'){
			$userid = $this->entrant->id;
			// check if user have uber voucher
			// if uber voucher is already there in list than no need to add it.
			// if user not having uber voucher than it will assign voucher to user.
			$isUserExist = VoucherModel::isWinUber($userid, true);
		}

		// find a subtype still available to assign
		if($voucherType == 'Uber'){
			$voucher = array('type'=>'Uber','subtype'=>'200');
			$voucher = (object)($voucher);
		}else{
			$subtypes = VoucherModel::getSubTypes($voucherType, true);
			
			// prepare list of options
			$subtypeOptions = [];
			foreach ($subtypes as $subtype) {
				$subtypeOptions[] = $subtype;
			}



			// chose an option if any are available, else set to null
			if (!empty($subtypeOptions)) {
				$chosenSubtype = $subtypeOptions[mt_rand(0, count($subtypeOptions) - 1)];
			} else {
				$chosenSubtype = null;
			}
			// load up the first of that type & subtype
			$voucher = VoucherModel::firstAvailableOfType($voucherType, $chosenSubtype);	

			// small catch for cases where there suddenly aren't any available
			if ($voucher === null) {
				return $this->chooseCompetition();
			}
		}	
		
		// assign one of those voucher codes to the entrant (breaks until mailers are fixed)
		$this->entrant->assignVoucher($voucher);

		// prepare a url slug
		$urlSlug = 'voucher/' . $voucher->type . '/' . $voucher->subtype;

		// get wheel index
		$wheelIndex = static::prizeIndexes('voucher')[$voucher->type];

		// return the prize response
		return $this->createPrizeResponse($urlSlug, $wheelIndex);
	}

	/**
	 * Create a list of options for competitions.
	 *
	 * @return array
	 */
	public function getCompetitionOptions()
	{
		// load up all competitions
		$competitions = CompetitionModel::all();

		

		// prepare an array of options with weights and wheel indexes
		$indexes = static::prizeIndexes('comp');



		$options = [];
		foreach ($competitions as $comp) {
			if (array_key_exists($comp->slug, $indexes)) {
				// add this competition as an option as many times as the weight dictates
				for ($i = 0; $i < $comp->weight; $i++) {
					$options[] = (object)[
						'wheelIndex' => $indexes[$comp->slug],
						'model' => $comp,
					];
				}
			}
		}
		return $options;
	}

	/**
	 * Get the necessary voucher counts to be used for calculating distribution.
	 *
	 * @return object
	 */
	protected function getVoucherCounts()
	{
		// start of day
		$todayStartString = date('Y-m-d');
		return (object)[
			'avail' => VoucherModel::countAvailOnDay($todayStartString),
			'won' => VoucherModel::countWonOnDay($todayStartString),
		];
	}

	/**
	 * Get the number of days left until the end of the competition.
	 * Includes the entirety of the current day, and counts until the end of the last day.
	 *
	 * @return integer
	 */
	public function getDaysTillEnd()
	{
		// get the end date option
		$endOption = \Option::get('site_settings', 'competition_end');

		// calculate number of days left until the end of the competition
		$endDate = new \DateTime($endOption == false ? '2016-06-01' : $endOption);
		// move end datetime to midnight of the final day
		$endDate->add(new \DateInterval('P1D'));
		// get todays datetime at the start of the day
		$todayDate = new \DateTime(date('Y-m-d'));

		// return the difference in days
		return $todayDate->diff($endDate)->days;
	}
}
