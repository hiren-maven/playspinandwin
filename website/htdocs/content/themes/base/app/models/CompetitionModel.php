<?php
namespace Model;

/**
 * CompetitionModel
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $details
 *
 * Relations:
 * @property \Illuminate\Database\Eloquent\Collection $entrants
 */
class CompetitionModel extends BaseModel
{
	/**
	 * string The wordpress post_type (optional)
	 */
	protected $wp_post_type = null;
	protected $table = 'wp_competitions';
	protected $primaryKey = 'id';
	const CREATED_AT = null;
	const UPDATED_AT = null;

	public $fillable = ['name', 'details'];

	public function entrants()
	{
		return $this->hasMany('Model\CompetitionEntrantModel', 'competition_id', 'id');
	}

	/**
	 * Find and populate an instance of CompetitionModel by slug.
	 *
	 * @param string $slug
	 * @return static
	 */
	public static function findBySlug($slug)
	{
		return static::where('slug', '=', $slug)->first();
	}
}