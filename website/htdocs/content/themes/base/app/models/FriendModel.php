<?php
namespace Model;

use Illuminate\Database\Eloquent\Builder;

/**
 * FriendModel
 *
 * @property integer $entrant_id
 * @property string $email
 * @property string $name
 * @property integer $entered
 */
class FriendModel extends BaseModel
{
	/**
	 * string The wordpress post_type (optional)
	 */
	protected $wp_post_type = null;
	protected $table = 'wp_competition_entrant_friends';
	protected $primaryKey = ['entrant_id', 'email'];
	public $incrementing = false;
	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'updated_date';

	public $fillable = ['entrant_id', 'email', 'name'];

	public function hasEntered()
	{
		$this->exists = true;
		$this->setAttribute('entered', 1);
		$this->save();
	}

	public static function findByEmail($email)
	{
		return static::where('email', '=', $email)->get()->all();
	}

	public static function findByEntrantAndEmail($entrant, $email)
	{
		return static::where('entrant_id', '=', $entrant->id)->where('email', '=', $email)->first();
	}

	public static function countByEntrant($entrant)
	{
		return static::where('entrant_id', '=', $entrant->id)->get()->count();
	}

	public static function countPendingByEntrant($entrant)
	{
		return static::where('entrant_id', '=', $entrant->id)->where('entered', "=", 0)->get()->count();
	}

	//check wether user entered into spin n win
	public static function countOfEntrant($email)
	{
		$cnt = static::where('email', '=', $email)->where('entered', "=", 1)->get()->count();
		return $cnt;
	}

	public static function entrantLimitReached($entrant)
	{
		$count = static::countByEntrant($entrant);
		return $count >= 3;
	}

	/**
	 * Set the keys for a save update query.
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	protected function setKeysForSaveQuery(Builder $query)
	{
		$key = $this->getKeyName();
		if (is_string($key)) {
			$key = [$key];
		}
		foreach ($key as $k) {
			$query->where($k, '=', $this->getKeyForSaveQuery($k));
		}

		return $query;
	}

	/**
	 * Get the primary key value for a save query.
	 *
	 * @param string $key
	 * @return mixed
	 */
	protected function getKeyForSaveQuery($key = null)
	{
		if (!$key) {
			$key = $this->getKeyName();
		}
		if (isset($this->original[$key])) {
			return $this->original[$key];
		}

		return $this->getAttribute($key);
	}

}
