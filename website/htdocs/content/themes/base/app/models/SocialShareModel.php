<?php
namespace Model;

/**
 * FriendModel
 *
 * @property integer $id
 * @property integer $entrant_id
 * @property string $platform
 * @property integer $earned_spin
 */
class SocialShareModel extends BaseModel
{
	/**
	 * string The wordpress post_type (optional)
	 */
	protected $wp_post_type = null;
	protected $table = 'wp_competition_social_shares';
	protected $primaryKey = 'id';
	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'updated_date';

	public $fillable = ['entrant_id', 'platform'];

	/**
	 * @param EntrantModel $entrant
	 * @param string $platform
	 * @return mixed
	 */
	public static function shared($entrant, $platform)
	{
		return (new static([
			'entrant_id' => $entrant->id,
			'platform' => $platform,
		]))->save();
	}

	/**
	 * Check if platform is accepted sharing platform.
	 *
	 * @param string $platform
	 * @return bool
	 */
	public static function isPlatform($platform)
	{
		return $platform === 'facebook' || $platform === 'twitter';
	}

	/**
	 * @param EntrantModel $entrant
	 * @return int
	 */
	public static function countByEntrant($entrant)
	{
		return static::where('entrant_id', '=', $entrant->id)->get()->count();
	}
}
