<?php
namespace Model;

/**
 * CompetitionEntrantModel
 *
 * @property integer $id
 * @property integer $competition_id
 * @property integer $entrant_id
 *
 * Relations:
 * @property CompetitionModel $competition
 */
class CompetitionEntrantModel extends BaseModel
{
	/**
	 * string The wordpress post_type (optional)
	 */
	protected $wp_post_type = null;
	protected $table = 'wp_competitions_entrants';
	protected $primaryKey = 'id';
	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'updated_date';

	public function competition()
	{
		return $this->belongsTo('Model\CompetitionModel', 'competition_id');
	}
}