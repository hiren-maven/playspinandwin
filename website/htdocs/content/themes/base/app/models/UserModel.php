<?php
namespace Model;

Class UserModel extends BaseModel{
    /**
     * string The wordpress post_type (optional)
     */
    protected $wp_post_type = null;
    protected $table = 'wp_users';
    protected $primaryKey = 'ID';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}