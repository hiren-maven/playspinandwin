<?php
namespace Model;

use WeDevs\ORM\Eloquent\Model as Eloquent;

class BaseModel extends Eloquent
{
    /**
    * string The wordpress post_type (optional)
    */
    protected $wp_post_type = null;
    protected $primaryKey = 'ID';
    public static $staticPrimaryKey = 'ID';
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';

    public $errors = [];

    public $defaultAttributes = [];

    /**
    * Extends newQuery to always filter for the model's wordpress post_type
    */
    public function newQuery(){
        $q = parent::newQuery();
        if (!empty($this->wp_post_type)){
            $q->where("post_type",$this->wp_post_type);
        }
        return $q;
    }

    /**
     * Validate the data.
     *
     * @param array $data
     * @param array $rules
     * @return bool
     */
    public function validate($data, $rules)
    {
        $v = new \Valitron\Validator($data);

        $v->addRule('notExists', function($field, $value, array $params) {
            $exists = static::query()->where($field, '=', $value)->get();
            return empty($exists->toArray());
        }, 'already exists');

        $v->addRule('emailExists', function($field, $value, array $params) {
            $result = BaseModel::emailExists($value, 'help@playspinandwin.co.za');
            return $result != 'invalid';
        }, 'doesn\'t exist');

        foreach ($rules as $rule => $attributes) {
            // attributes can be a string, array of strings, or array of objects
            // the first two can be passed in the same way
            // the last must be parsed for parameters
            if (is_object(reset($attributes))) {
                // object format: attribute -> ATTR_NAME, params -> PARAMETERS
                foreach ($attributes as $config) {
                    $v->rule($rule, $config->attribute, $config->params);
                }
            } else {
                $v->rule($rule, $attributes);
            }
        }

        if ($v->validate()) {
            return true;
        } else {
            $this->errors = $v->errors();
            return false;
        }
    }

    public function fill(array $attributes)
    {
        $model = parent::fill($attributes);

        foreach ($this->defaultAttributes as $attr => $value) {
            if (!$model->$attr) {
                $model->setAttribute($attr, $value);
            }
        }

        return $model;
    }

    /**
     * Set attributes for the model instance.
     *
     * @param array $attributes
     * @return static
     */
    public function setAttributes($attributes)
    {
        foreach ($attributes as $k => $v) {
            $this->setAttribute($k, $v);
        }

        return $this;
    }

    /**
     * Find and populate an instance of BaseModel by primaryKey.
     *
     * @param string|integer $primaryKey
     * @return static
     */
    public static function findById($primaryKey)
    {
        return static::query()->where(static::$staticPrimaryKey, '=', $primaryKey)->first();
    }

    /**
     * Add a basic where clause to the query.
     *
     * @param string|\Closure $column
     * @param string $operator
     * @param mixed $value
     * @param string $boolean
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return static::query()->where($column, $operator, $value, $boolean);
    }

    /**
     * Given an array of arrays of attributes, this will return a number of populated models.
     *
     * @param array $attributes
     * @return array
     */
    public static function populateModels($attributes)
    {
        $models = [];
        foreach ($attributes as $k => $attr) {
            $models[] = static::populateModel($attr);
        }
        return $models;
    }

    /**
     * Populate a model instance with attributes.
     *
     * @param array $attributes
     * @return static
     */
    public static function populateModel($attributes)
    {
        return (new static)->setAttributes($attributes);
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $template
     * @param array $params
     * @param array $headers
     * @return bool
     */
    protected function sendEmail($to, $subject, $template, $params = [], $headers = [])
    {
        /*echo '<pre>';
        print_r($template);
        print_r($params);
        echo ' to '.$to;
        echo ' subject '.$subject; 
        die;*/
        // get the html body using the [[View]] class
        $body = \View::make($template, $params)->render();

        // set a from header
        $headers[] = 'From: "Spin & Win" <help@playspinandwin.co.za>';
        // set an html content-type header, note: this must be AFTER the from header for some reason
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        // add sub-account headers
        //$headers[] = 'X-MC-Subaccount: playspinandwin';

        // send the email, and return it's success status
        return wp_mail($to, $subject, $body, $headers);
    }

    /**
     * @param string $toemail
     * @param string $fromemail
     * @param bool $getdetails
     * @return array|string
     */
    public static function emailExists($toemail, $fromemail, $getdetails = false)
    {
        $details = '';
        $result = '';
        $mxweight = [];
        $email_arr = explode("@", $toemail);
        $domain = array_slice($email_arr, -1);
        $domain = $domain[0];
        // Trim [ and ] from beginning and end of domain string, respectively
        $domain = ltrim($domain, "[");
        $domain = rtrim($domain, "]");

        if ("IPv6:" == substr($domain, 0, strlen("IPv6:"))) {
            $domain = substr($domain, strlen("IPv6") + 1);
        }
        $mxhosts = array();
        if (filter_var($domain, FILTER_VALIDATE_IP)) {
            $mx_ip = $domain;
        } else {
            getmxrr($domain, $mxhosts, $mxweight);
        }
        if (!empty($mxhosts)) {
            $mx_ip = $mxhosts[array_search(min($mxweight), $mxhosts)];
        } else {
            if (filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $record_a = dns_get_record($domain, DNS_A);
            }
            elseif (filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                $record_a = dns_get_record($domain, DNS_AAAA);
            }
            if (!empty($record_a)) {
                $mx_ip = $record_a[0]['ip'];
            } else {
                $result   = "invalid";
                $details .= "No suitable MX records found.";
                return ( (true == $getdetails) ? array($result, $details) : $result );
            }
        }
        $connect = @fsockopen($mx_ip, 25);
        if ($connect) {
            if (preg_match("/^220/i", $out = fgets($connect, 1024))) {
                fputs ($connect , "HELO $mx_ip\r\n");
                $out = fgets ($connect, 1024);
                $details .= $out."\n";

                fputs ($connect , "MAIL FROM: <$fromemail>\r\n");
                $from = fgets ($connect, 1024);
                $details .= $from."\n";
                fputs ($connect , "RCPT TO: <$toemail>\r\n");
                $to = fgets ($connect, 1024);
                $details .= $to."\n";
                fputs ($connect , "QUIT");
                fclose($connect);
                if (!preg_match("/^250/i", $from) || !preg_match("/^250/i", $to)) {
                    if (
                        $domain == 'yahoo.com' &&
                        ($to == false || preg_match("/^45[0-9]/i", $to) || preg_match("/^421/i", $from))
                    ) {
                        $result = 'yahoovalid';
                    } else {
                        $result = "invalid";
                    }
                } else {
                    $result = "valid";
                }
            }
        } else {
            $result = "invalid";
            $details .= "Could not connect to server";
        }

        if ($getdetails) {
            return (object) array(
                'result' => $result,
                'details' => $details,
            );
        } else {
            return $result;
        }
    }
}
