<?php
namespace Model;

/**
 * VoucherModel
 *
 * @property integer $id
 * @property string $code
 * @property string $type
 * @property string $subtype
 * @property string $value
 * @property integer $winner_id
 */
class VoucherModel extends BaseModel
{
	/**
	 * string The wordpress post_type (optional)
	 */
	protected $wp_post_type = null;
	/**
	 * @var string $table the database table used to store vouchers.
	 * @see static::countByType() if you change this value.
	 */
	protected $table = 'wp_competition_vouchers';
	protected $primaryKey = 'id';
	public static $staticPrimaryKey = 'id';
	const CREATED_AT = null;
	const UPDATED_AT = null;

	/**
	 * Get the first available voucher of the specified type.
	 *
	 * @param string $type
	 * @param string $subType
	 * @return static
	 */
	public static function firstAvailableOfType($type, $subType = null)
	{
		 $qry = static::where('type', '=', $type)->where('subtype', '=', $subType)->where('winner_id', '=', null)->first();
		return $qry;
	}

	/**
	 * Count vouchers, grouped by type and subtype.
	 *
	 * @param bool $available whether to only get available vouchers. Defaults to false.
	 * @return array|null|object
	 */
	public static function countByTypeAndSubtype($available = false)
	{
		global $wpdb;

		// raw query as this is a unique requirement
		// fortunately there are no security flaws so long as there is no user input :)
		// also a bit shitty that $this->table is not static... MEH
		return $wpdb->get_results('SELECT type, subtype, COUNT(*) FROM wp_competition_vouchers' . ($available ? ' WHERE `winner_id` IS NULL ' : ' ') . 'GROUP BY type, subtype', OBJECT_K);
	}

	/**
	 * @param string $type
	 * @param bool $available whether to only get available vouchers. Defaults to false.
	 * @return array|null|object
	 */
	public static function getSubTypes($type, $available = false)
	{
		global $wpdb;

		// raw query as this is a unique requirement
		// fortunately there are no security flaws so long as there is no user input :)
		// also a bit shitty that $this->table is not static... MEH
		$qry = $wpdb->get_results('SELECT DISTINCT(subtype) FROM wp_competition_vouchers WHERE `type` = \'' . $type . '\'' . ($available ? ' AND `winner_id` IS NULL' : ''), ARRAY_N);

		return $qry;
	}

	/**
	 * @param string $dayString
	 * @return object
	 */
	public static function countAvailOnDay($dayString)
	{
		global $wpdb;

		// raw query as this is a unique requirement
		// fortunately there are no security flaws so long as there is no user input :)
		// also a bit shitty that $this->table is not static... MEH

		// all vouchers that are/were available form the start of the day
		$availOnDay = $wpdb->get_results('SELECT type, COUNT(*) FROM wp_competition_vouchers WHERE `winner_id` IS NULL OR `won_at` >= \'' . $dayString . '\' GROUP BY type', OBJECT_K);
		// all vouchers that were given away on the day

		$availOnDay['Uber'] = array('type' => 'Uber','COUNT(*)'=>'Unlimited');

		$availOnDay['Uber'] = (object)$availOnDay['Uber'];

		
		return $availOnDay;
	}

	/**
	 * @param string $dayString
	 * @return object
	 */
	public static function countWonOnDay($dayString)
	{
		global $wpdb;

		// raw query as this is a unique requirement
		// fortunately there are no security flaws so long as there is no user input :)
		// also a bit shitty that $this->table is not static... MEH

		// all vouchers that were given away on the day
		$wonOnDay = $wpdb->get_results('SELECT type, COUNT(*) FROM wp_competition_vouchers WHERE `won_at` >= \'' . $dayString . '\' AND `won_at` <= \'' . ($dayString . ' 23:59:59') . '\' GROUP BY type', OBJECT_K);

		return $wonOnDay;
	}


	/**
	 * @param string $userid
	 * @return object
	 */
	public static function isWinUber($userid)
	{
		global $wpdb;

		// raw query as this is a unique requirement
		// fortunately there are no security flaws so long as there is no user input :)
		// also a bit shitty that $this->table is not static... MEH

		// check if user have uber voucher or not
		$isUserWin = $wpdb->get_results('SELECT type, COUNT(*) as cnt FROM wp_competition_vouchers WHERE `winner_id` =  \'' . $userid . '\' ', ARRAY_A);

		//check if user have voucher or not
		if ($isUserWin[0]['cnt'] == 0) {
			//insert uber voucher for user
			$insrtdata = array( 
				'code' => 'SPINWIN',
				'type'=>'Uber',
				'subtype'=>'200',
				'value'=>'200', 
				'winner_id' => $userid,
				'won_at'=>date('Y-m-d H:i:s')
			);
			$wpdb->insert('wp_competition_vouchers', $insrtdata);
		}
		return $isUserWin[0]['cnt'];
	}

}
